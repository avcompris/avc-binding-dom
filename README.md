# About avc-binding-dom

This is a simple API for binding Java interfaces to XML documents and nodes (DOM).

We use Java 5's annotations.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Project dependencies include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)

Projects which depend on this one include:

  * [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)

This is the project home page, hosted on GitLab.

See the
[Installation Guide](https://maven.avcompris.com/avc-binding-dom/Install.html)
and the
["Getting Started"](https://maven.avcompris.com/avc-binding-dom/GettingStarted.html)
2-minute tutorial.

[API Documentation is here](https://maven.avcompris.com/avc-binding-dom/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-binding-dom/)
