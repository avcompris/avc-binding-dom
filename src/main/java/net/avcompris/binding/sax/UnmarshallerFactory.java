package net.avcompris.binding.sax;

import net.avcompris.binding.BindConfiguration;

import org.xml.sax.ContentHandler;

/**
 * a SAX binding strategy can be 
 * viewed as a way of serving given implementations of
 * {@link ContentHandler} depenging on the interface to bind.
 * 
 * @author David Andrianavalontsalama
 */
public interface UnmarshallerFactory {

	<T> Unmarshaller<T> createUnmarshaller(BindConfiguration configuration,
			ClassLoader classLoader, Class<T> clazz);
}
