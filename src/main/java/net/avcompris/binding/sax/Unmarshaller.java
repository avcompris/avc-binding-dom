package net.avcompris.binding.sax;

import org.xml.sax.ContentHandler;

/**
 * the result of a <code>SAXBinder</code> strategy.
 * Name is based on Sun's
 * <code>javax.xml.bind.UnmarshallerHandler</code>.
 * 
 * @author David Andrianavalontsalama
 */
public interface Unmarshaller<T> extends ContentHandler {

	T getResult();
}
