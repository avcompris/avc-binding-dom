package net.avcompris.binding.sax;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.Binder;

import org.xml.sax.InputSource;

public interface SAXBinder extends Binder<InputSource> {

	/**
	 * bidirectly bind a Java dynamic proxy to a XML source,
	 * using the XPath expression declared as an annotation in the 
	 * <code>clazz</code> interface.
	 */
	@Override
	<T> T bind(InputSource input, Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to XML source,
	 * using the XPath expression declared as an annotation in the 
	 * <code>clazz</code> interface
	 * and the {@link ClassLoader} passed as a parameter.	 
	 */
	@Override
	<T> T bind(InputSource input, ClassLoader classLoader, Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to a XML source,
	 * using the XPath expression passed via the
	 * <code>configuration</code> parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, InputSource input,
			Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to a XML source,
	 * using the XPath expression
	 * and the {@link ClassLoader} passed via the
	 * <code>configuration</code> parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, InputSource input,
			ClassLoader classLoader, Class<T> clazz);
}
