package net.avcompris.binding.sax;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static com.google.common.primitives.Primitives.isWrapperType;

import java.lang.reflect.Array;

import com.avcompris.util.AbstractUtils;

//TODO this class belongs elsewhere? binding-common?!
public abstract class CastUtils extends AbstractUtils {

	public static boolean isSimpleType(final Class<?> type) {

		nonNullArgument(type, "type");

		if (String.class.equals(type)) {

			return true;
		}

		return type.isPrimitive() || isWrapperType(type);
	}

	public static Object incArray(final Class<?> componentType,
			final Object oldArray, final Object newItem) {

		nonNullArgument(componentType, "componentType");
		nonNullArgument(oldArray, "oldArray");
		nonNullArgument(newItem, "newItem");

		final int length = Array.getLength(oldArray);

		final Object array = Array.newInstance(componentType, length + 1);

		for (int i = 0; i < length; ++i) {

			Array.set(array, i, Array.get(oldArray, i));
		}

		Array.set(array, length, newItem);

		return array;
	}
}
