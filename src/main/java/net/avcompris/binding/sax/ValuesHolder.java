package net.avcompris.binding.sax;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.lang.NotImplementedException;

 final class ValuesHolder implements InvocationHandler {

	public ValuesHolder(
			final ClassLoader classLoader,
			final Class<?> clazz) {

		nonNullArgument(classLoader, "classLoader");
		nonNullArgument(clazz, "clazz");

		proxy = Proxy.newProxyInstance(classLoader, new Class<?>[]{
			clazz
		}, this);
	}

	private final Object proxy;

	public Object getObjectProxy() {

		return proxy;
	}

	public void setValue(final Method method, final Object value) {

		nonNullArgument(method, "method");
		nonNullArgument(value, "value");

		values.put(method.getName(), value);
	}

	private final Map<String, Object> values = new HashMap<String, Object>();

	@Override
	public Object invoke(final Object proxy, final Method method,
			@Nullable final Object[] args) throws Throwable {

		nonNullArgument(proxy, "proxy");
		nonNullArgument(method, "method");

		if (args != null && args.length != 0) {
			throw new NotImplementedException("Method has parameters ("
					+ args.length + "): " + method);
		}

		return values.get(method.getName());
	}
}
