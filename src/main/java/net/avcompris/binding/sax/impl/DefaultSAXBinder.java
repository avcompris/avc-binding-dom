package net.avcompris.binding.sax.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.sax.Unmarshaller;

public class DefaultSAXBinder extends AbstractSAXBinder {

	@Override
	public <T> Unmarshaller<T> createUnmarshaller(
			final BindConfiguration configuration,
			final ClassLoader classLoader, final Class<T> clazz) {

		nonNullArgument(configuration,"configuration");
		nonNullArgument(classLoader,"classLoader");
		nonNullArgument(clazz,"clazz");

		return new DefaultUnmarshaller<T>(configuration, classLoader, clazz);
	}
}
