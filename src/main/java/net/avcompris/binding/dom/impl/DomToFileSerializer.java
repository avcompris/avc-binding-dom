package net.avcompris.binding.dom.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.io.File;
import java.io.Writer;

import net.avcompris.binding.impl.AbstractToFileSerializer;
import net.avcompris.binding.impl.AbstractToWriterSerializer;

import org.w3c.dom.Node;

public class DomToFileSerializer extends AbstractToFileSerializer<Node> {

	public DomToFileSerializer(final File file) {

		super(file);
	}

	@Override
	protected AbstractToWriterSerializer<Node> createToWriterSerializer(
			final Writer writer) {

		nonNullArgument(writer, "writer");

		return new DomToWriterSerializer(writer);
	}
}
