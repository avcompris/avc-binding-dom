package net.avcompris.binding.dom.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathVariableResolver;

import net.avcompris.binding.impl.BinderXPathVariableResolver;

import com.avcompris.common.annotation.Nullable;

/**
 * bridge between the variable resolver provided by avc-binding-common, and
 * the javax' XPath implementation.
 *
 * @author David Andrianavalontsalama
 */
final class JaxpDomBinderXPathVariableResolver implements
		XPathVariableResolver {

	public JaxpDomBinderXPathVariableResolver(
			final BinderXPathVariableResolver resolver) {

		this.resolver = nonNullArgument(resolver, "resolver");
	}

	private final BinderXPathVariableResolver resolver;

	@Override
	public Object resolveVariable(@Nullable final QName variableName) {

		return resolver.resolveVariable(variableName);
	}
}
