package net.avcompris.binding.dom.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.lang.reflect.Proxy;

import javax.xml.xpath.XPathFactory;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.impl.AbstractBinder;
import net.avcompris.binding.impl.AbstractBinderInvocationHandler;

import org.w3c.dom.Node;

/**
 * default implementation of {@link DomBinder}.
 *
 * @author David Andrianavalontsalama
 */
public class JaxpDomBinder extends AbstractBinder<Node> implements DomBinder {

	/**
	 * default constructor: Uses {@link 
	 * XPathFactory#newInstance()}.
	 */
	public JaxpDomBinder() {

		this(XPathFactory.newInstance());
	}

	public JaxpDomBinder(final XPathFactory xpathFactory) {

		this.xpathFactory = nonNullArgument(xpathFactory, "xpathFactory");
	}

	private final XPathFactory xpathFactory;

	@Override
	protected final <T> T bindInterface(final BindConfiguration configuration,
			final Node node, final ClassLoader classLoader, final Class<T> clazz) {

		nonNullArgument(clazz, "clazz");
		nonNullArgument(node, "node");
		nonNullArgument(configuration, "configuration");
		nonNullArgument(classLoader, "classLoader");

		final AbstractBinderInvocationHandler<Node> invocationHandler = new JaxpDomBinderInvocationHandler(
				getThis(), classLoader, clazz, node, xpathFactory,
				configuration);

		final Object proxy = Proxy.newProxyInstance(classLoader,
				new Class<?>[]{
					clazz
				}, invocationHandler);

		return clazz.cast(proxy);
	}
}
