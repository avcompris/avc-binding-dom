package net.avcompris.binding.dom.impl;

import net.avcompris.binding.dom.DomBinder;

/**
 * default implementation of {@link DomBinder}:
 * Actually it is {@link JaxenDomBinder}.
 *
 * @author David Andrianavalontsalama
 */
public final class DefaultDomBinder extends JaxenDomBinder {

	/**
	 * get a singleton instance of this binder class.
	 */
	public static DomBinder getInstance() {

		if (instance == null) {

			loadInstance();
		}

		return instance;
	}

	/**
	 * the singleton instance of this binder class, loaded lazily.
	 */
	private static DomBinder instance = null;

	/**
	 * load the singleton instance.
	 */
	private static synchronized void loadInstance() {

		if (instance != null) {

			return;
		}

		instance = new DefaultDomBinder();
	}
}
