package net.avcompris.binding.dom.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.dom.DomBinderFilter;
import net.avcompris.binding.impl.UniqueBinder;

import org.w3c.dom.Node;

public class DomUniqueBinder extends UniqueBinder<Node> implements
		DomBinderFilter {

	public DomUniqueBinder(final DomBinder delegate) {

		super(delegate);

		this.delegate = nonNullArgument(delegate, "delegate");
	}

	private final DomBinder delegate;

	@Override
	public DomBinder getDelegate() {

		return delegate;
	}
}
