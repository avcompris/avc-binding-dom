package net.avcompris.binding.dom.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.lang.reflect.Proxy;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.impl.AbstractBinder;
import net.avcompris.binding.impl.AbstractBinderInvocationHandler;

import org.w3c.dom.Node;

public class JaxenDomBinder  extends AbstractBinder<Node> implements DomBinder {

	@Override
	protected final <T> T bindInterface(final BindConfiguration configuration,
			final Node node, final ClassLoader classLoader, final Class<T> clazz) {

		nonNullArgument(clazz, "clazz");
		nonNullArgument(node, "node");
		nonNullArgument(configuration, "configuration");
		nonNullArgument(classLoader, "classLoader");

		final AbstractBinderInvocationHandler<Node> invocationHandler = new JaxenDomBinderInvocationHandler(
				getThis(), classLoader, clazz, node, configuration);

		final Object proxy = Proxy.newProxyInstance(classLoader,
				new Class<?>[]{
					clazz
				}, invocationHandler);

		return clazz.cast(proxy);
	}
}
