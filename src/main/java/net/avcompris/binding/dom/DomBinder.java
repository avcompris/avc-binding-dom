package net.avcompris.binding.dom;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.Binder;

import org.w3c.dom.Node;

/**
 * a Binder is responsible for binding a Java dynamix proxy to a DOM node,
 * et recriprocally.
 *
 * @author David Andrianavalontsalama
 */
public interface DomBinder extends Binder<Node> {

	/**
	 * bidirectly bind a Java dynamic proxy to a DOM node,
	 * using the XPath expression declared as an annotation in the 
	 * <code>clazz</code> interface.
	 */
	@Override
	<T> T bind(Node node, Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to a DOM node,
	 * using the XPath expression declared as an annotation in the 
	 * <code>clazz</code> interface
	 * and the {@link ClassLoader} passed as a parameter.	 
	 */
	@Override
	<T> T bind(Node node, ClassLoader classLoader, Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to a DOM node,
	 * using the XPath expression passed via the
	 * <code>configuration</code> parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Node node, Class<T> clazz);

	/**
	 * bidirectly bind a Java dynamic proxy to a DOM node,
	 * using the XPath expression
	 * and the {@link ClassLoader} passed via the
	 * <code>configuration</code> parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Node node,
			ClassLoader classLoader, Class<T> clazz);
}
