package net.avcompris.binding.dom;

import net.avcompris.binding.BindingException;

import com.avcompris.common.annotation.Nullable;

/**
 * This exception will be thrown when a call to a method bound
 * should encounter an error due to an XPath expression. 
 */
public class DomBindingException extends BindingException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = -2753791040153911473L;

	/**
	 * constructor.
	 */
	public DomBindingException(
			@Nullable final String xpath,
			@Nullable final Throwable cause) {

		super(xpath, cause);
	}
}
