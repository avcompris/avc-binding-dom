package net.avcompris.binding.dom;

import net.avcompris.binding.BinderFilter;

import org.w3c.dom.Node;

public interface DomBinderFilter extends DomBinder, BinderFilter<Node> {

	@Override
	DomBinder getDelegate();
}
