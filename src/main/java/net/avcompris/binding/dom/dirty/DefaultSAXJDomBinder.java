package net.avcompris.binding.dom.dirty;

import net.avcompris.binding.sax.impl.DefaultSAXBinder;


public class DefaultSAXJDomBinder extends DirtySAXJDomBinder {

	public DefaultSAXJDomBinder() {

		super(new DefaultSAXBinder());
	}
}
