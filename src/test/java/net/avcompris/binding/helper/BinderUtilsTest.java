package net.avcompris.binding.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Proxy;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.binding.impl.AbstractBinderInvocationHandler;

import org.junit.Test;

public class BinderUtilsTest {

	@Test
	public void testDetach() throws Exception {

		final Dummy foo1 = DomBinderUtils.xmlContentToJava(
				"<foo>Hello World!</foo>", Dummy.class);

		assertEquals("Hello World!", foo1.getText());

		assertEquals("foo", foo1.getNodeName());

		assertTrue(AbstractBinderInvocationHandler.class.isInstance(Proxy
				.getInvocationHandler(foo1)));

		assertEquals(foo1, foo1);

		final Dummy foo2 = BinderUtils.detach(foo1);

		assertEquals("Hello World!", foo2.getText());

		assertEquals("foo", foo2.getNodeName());

		assertNotSame(foo1, foo2);

		assertFalse(AbstractBinderInvocationHandler.class.isInstance(Proxy
				.getInvocationHandler(foo2)));

		assertEquals(foo2, foo2);

		assertEquals(foo1, foo2);

		assertEquals(foo2, foo1);

		final Dummy foo3 = BinderUtils.detach(foo2);

		assertSame(foo2, foo3);

		foo1.setText("Good Bye");

		assertNotEquals(foo1, foo2);

		assertNotEquals(foo2, foo1);
	}


	@Test
	public void testDetach_String() throws Exception {
		
		final String toto = "toto";
		
		final String toto2 = BinderUtils.detach(toto);
		
		assertSame(toto, toto2);
	}

	@Test(expected = NullPointerException.class)
	public void test_nullField_notNullable_crash() throws Exception {

		final DummyNullField foo = DomBinderUtils.xmlContentToJava(
				"<foo>Hello World!</foo>", DummyNullField.class);

		foo.getAbsent();
	}

	@Test
	public void testDetach_nullField_notNullable() throws Exception {

		final DummyNullField foo1 = DomBinderUtils.xmlContentToJava(
				"<foo>Hello World!</foo>", DummyNullField.class);

		assertEquals(foo1, foo1);

		BinderUtils.detach(foo1);
	}

	@Test
	public void testDetach_nullField_nullable() throws Exception {

		final DummyNullField_nullable foo1 = DomBinderUtils.xmlContentToJava(
				"<foo>Hello World!</foo>", DummyNullField_nullable.class);

		assertEquals(foo1, foo1);

		BinderUtils.detach(foo1);
	}
}

@XPath("/*")
interface Dummy {

	@XPath(".")
	String getText();

	void setText(String text);

	@XPath("name()")
	String getNodeName();

	@XPath("concat(name(), ': ', .)")
	@Override
	String toString();
}

@XPath("/*")
interface DummyNullField extends Dummy {

	@XPath("absent")
	NullField getAbsent();

	boolean isNullAbsent();

	interface NullField {

	}
}

@XPath("/*")
interface DummyNullField_nullable extends DummyNullField {

	@XPath("absent")
	@Nullable
	@Override
	NullField getAbsent();
}