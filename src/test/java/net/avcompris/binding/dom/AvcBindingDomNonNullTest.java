package net.avcompris.binding.dom;

import java.util.Collection;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.impl.DefaultDomBinder;
import net.avcompris.binding.dom.impl.DomUniqueBinder;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.reflect.AbstractNonNullTest;
import com.avcompris.util.reflect.UseInstance;

/**
 * tests on Java classes in the "<code>net.avcompris.binding.dom</code>" package, to check
 * if the method parameters that are not flagged
 * as {@link Nullable} are checked for <code>null</code>-values.
 * 
 * @author David Andrianavalontsalama 2011 ©
 */
@RunWith(AvcParameterized.class)
public class AvcBindingDomNonNullTest extends AbstractNonNullTest {

	/**
	 * constructor.
	 * 
	 * @param holder the instance+member to test
	 */
	public AvcBindingDomNonNullTest(final MemberHolder holder) throws Exception {

		super(holder);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		final DomBinder defaultDomBinder = new DefaultDomBinder();

		return parametersScanProject(DomBindingException.class,
				defaultDomBinder, BindConfiguration.newBuilder().build(),
				UseInstance.forClass(ClassLoader.class, Empty.class
						.getClassLoader(), false), UseInstance.forClass(
						Class.class, Empty.class, false), new DomUniqueBinder(
						defaultDomBinder));
	}

	@XPath("/*")
	public interface Empty {

		// empty
	}
}
