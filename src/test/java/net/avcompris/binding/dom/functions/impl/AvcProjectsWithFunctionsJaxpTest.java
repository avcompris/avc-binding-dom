package net.avcompris.binding.dom.functions.impl;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.functions.AvcProjectsWithFunctionsTest;
import net.avcompris.binding.dom.impl.JaxpDomBinder;

@DomBinderImplClass(JaxpDomBinder.class)
public class AvcProjectsWithFunctionsJaxpTest extends
		AvcProjectsWithFunctionsTest {

	// empty class
}
