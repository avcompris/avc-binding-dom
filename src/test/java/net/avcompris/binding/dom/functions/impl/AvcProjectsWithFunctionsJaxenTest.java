package net.avcompris.binding.dom.functions.impl;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.functions.AvcProjectsWithFunctionsTest;
import net.avcompris.binding.dom.impl.JaxenDomBinder;

@DomBinderImplClass(JaxenDomBinder.class)
public class AvcProjectsWithFunctionsJaxenTest extends
		AvcProjectsWithFunctionsTest {

	// empty class
}
