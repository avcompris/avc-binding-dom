package net.avcompris.binding.dom.functions;

import net.avcompris.binding.BindFunctions;
import net.avcompris.binding.annotation.XPathFunctionNames;

import org.w3c.dom.Node;

import com.avcompris.common.annotation.Nullable;

public class AvcProjectsStringFunctions implements BindFunctions {

	@Override
	public boolean matchesNamespaceURI(final String uri) {

		return true;
	}

	public String reverseString(@Nullable final Node currentNode,
			@Nullable final String s) {

		if (s == null) {
			return null;
		}

		final char[] chars = new char[s.length()];
		
		for (int i = 0; i<chars.length;++i) {
			
			chars[i]=s.charAt(chars.length-1-i);
		}
		
		return new String(chars);		
	}


	@XPathFunctionNames({"actually-not-used", "funny-is-it-not"})
	public String reverseString(@Nullable final Node currentNode) {

		final String value = (currentNode == null) ? null : currentNode
				.getNodeValue();

		return reverseString(currentNode, value);
	}
}
