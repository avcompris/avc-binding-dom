package net.avcompris.binding.dom.functions;

import net.avcompris.binding.BindFunctions;
import net.avcompris.binding.annotation.XPathFunctionNames;

import org.w3c.dom.Node;

import com.avcompris.common.annotation.Nullable;

public class AvcProjectsFunctions implements BindFunctions {

	@Override
	public boolean matchesNamespaceURI(final String namespaceURI) {

		return true;
	}

	@XPathFunctionNames("get-hg-repository")
	public String getMercurialRepoUrl(@Nullable final Node node) {

		if (node == null) {
			return "";
		}

		return node.getAttributes().getNamedItem("repository").getNodeValue();
	}
}
