package net.avcompris.binding.dom.functions;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;

@DomBinderImplClass(DefaultDomBinder.class)
public class AvcProjectsWithFunctionsTest extends AbstractDomBinderTest {

	@Test
	public final void testBase() throws Exception {

		final AvcProjectsWithFunctions a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithFunctions.class);

		final String[] repositoryUrls = a.getRepositoryUrls();
		
		assertEquals(5, repositoryUrls.length);

		assertEquals("https://${avc.project}.googlecode.com/hg", repositoryUrls[1]);
	}

	@Test
	public final void test_get_reverse_hg_repository() throws Exception {

		final AvcProjectsWithFunctions a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithFunctions.class);

		final String[] repositoryUrls = a.getReverseRepositoryUrlsThroughFunction();
		
		assertEquals(5, repositoryUrls.length);

		assertEquals("gh/moc.edocelgoog.}tcejorp.cva{$//:sptth", repositoryUrls[1]);
	}

	@Test
	public final void test_get_reverse() throws Exception {

		final AvcProjectsWithFunctions a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithFunctions.class);

		final String repositoryUrl1 = a.getReverseRepositoryUrl(1);
		
		assertEquals("gh/moc.edocelgoog.}tcejorp.cva{$//:sptth", repositoryUrl1);
	}

	@Test
	public final void test_get_hg_repository() throws Exception {

		final AvcProjectsWithFunctions a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithFunctions.class);

		final String[] repositoryUrls = a.getRepositoryUrlsThroughFunction();
		
		assertEquals(5, repositoryUrls.length);

		assertEquals("https://${avc.project}.googlecode.com/hg", repositoryUrls[1]);
	}
}
