package net.avcompris.binding.dom.functions;

import net.avcompris.binding.annotation.Functions;
import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;

@XPath(value = "/avc-workspace")
@Functions({
		AvcProjectsStringFunctions.class, AvcProjectsFunctions.class
})
@Namespaces("xmlns:my=MyNs")
public interface AvcProjectsWithFunctions {

	@XPath(value = "hg", function = "@repository")
	String[] getRepositoryUrls();

	@XPath(value = "hg[count(preceding-sibling::hg) = $arg0]/@repository",			
			function = "my:reverseString()")
	String getReverseRepositoryUrl(int index);

	@XPath(value = "hg", function = "my:get-hg-repository()")
	String[] getRepositoryUrlsThroughFunction();

	@XPath(value = "hg", function = "my:reverseString(my:get-hg-repository())")
	String[] getReverseRepositoryUrlsThroughFunction();
}
