package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

@DomBinderImplClass(DefaultDomBinder.class)
public class FetchedLogsImplDefaultDomBinderTest extends AbstractFetchedLogsImpls_NotSame_Test{

	// empty class
}
