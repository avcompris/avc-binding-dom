package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DomUniqueBinder;
import net.avcompris.binding.dom.impl.JaxenDomBinder;

@DomBinderImplClass(value = JaxenDomBinder.class, filters = DomUniqueBinder.class)
public class FetchedLogsImplJaxenDomBinder_Unique_Test extends
		AbstractFetchedLogsImplsTest {

	// empty class
}
