package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;

@DomBinderImplClass(value = JaxpDomBinder.class)
public class FetchedLogsImplJaxpDomBinderTest extends
		AbstractFetchedLogsImpls_NotSame_Test {

	// empty class
}
