package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;

@DomBinderImplClass(JaxenDomBinder.class)
public class FetchedLogsImplJaxenDomBinderTest extends
		AbstractFetchedLogsImpls_NotSame_Test {

	// empty class
}
