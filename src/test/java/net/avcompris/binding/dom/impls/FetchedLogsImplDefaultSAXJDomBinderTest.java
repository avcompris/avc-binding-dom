package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.dirty.DefaultSAXJDomBinder;

import org.junit.Ignore;

@Ignore
@DomBinderImplClass(DefaultSAXJDomBinder.class)
public class FetchedLogsImplDefaultSAXJDomBinderTest extends
		AbstractFetchedLogsImplsTest {

	// empty class
}
