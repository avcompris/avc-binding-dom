package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DomUniqueBinder;
import net.avcompris.binding.dom.impl.JaxpDomBinder;

@DomBinderImplClass(value = JaxpDomBinder.class, filters = DomUniqueBinder.class)
public class FetchedLogsImplJaxpDomBinder_Unique_Test extends
		AbstractFetchedLogsImplsTest {

	// empty class
}
