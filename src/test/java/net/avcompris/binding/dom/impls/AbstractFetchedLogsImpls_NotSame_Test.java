package net.avcompris.binding.dom.impls;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertNotSame;
import net.avcompris.binding.dom.logs.FetchedLogLine;
import net.avcompris.binding.dom.logs.FetchedLogs;

import org.junit.Test;

public abstract class AbstractFetchedLogsImpls_NotSame_Test extends
		AbstractFetchedLogsImplsTest {

	@Test
	public final void test_Aliases_notSame() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		System.out.println(log_0.getUUID());

		System.out.println(log_0_0.getUUID());

		assertNotSame(log_0, log_0_0);
	}
}
