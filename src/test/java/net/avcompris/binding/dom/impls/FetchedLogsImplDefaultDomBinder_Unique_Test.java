package net.avcompris.binding.dom.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;
import net.avcompris.binding.dom.impl.DomUniqueBinder;

@DomBinderImplClass(value=DefaultDomBinder.class, filters=DomUniqueBinder.class)
public class FetchedLogsImplDefaultDomBinder_Unique_Test extends
		AbstractFetchedLogsImplsTest {

	// empty class
}
