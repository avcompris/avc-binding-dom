package net.avcompris.binding.dom.jenkinsJobConfig;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Before;
import org.junit.Test;

@DomBinderImplClass(DefaultDomBinder.class)
public class JenkinsJobConfigTest extends AbstractDomBinderTest {

	@Before
	public void setUp() {

		job = DomBinderUtils.xmlContentToJava(new File(
				"src/test/xml/007-jenkinsJob-config.xml"),
				JenkinsJobConfig.class);
	}

	private JenkinsJobConfig job;

	@Test
	public void test_loadInnerInterfaces_and_attributes() throws Exception {

		assertFalse(job.isNullCleanBeforeCheckout());
	}

	@Test
	public void test_setDisabledTrue() {

		assertFalse(job.isDisabled());

		job.setDisabled(true);

		assertTrue(job.isDisabled());
	}

	@Test
	public void test_setDisabledFalse() {

		assertFalse(job.isDisabled());

		job.setDisabled(false);

		assertFalse(job.isDisabled());
	}
}

@XPath("/*")
interface JenkinsJobConfig {

	@XPath("scm/extensions/hudson.plugins.git.extensions.impl.CleanBeforeCheckout")
	boolean isNullCleanBeforeCheckout();

	@XPath("disabled = 'true'")
	boolean isDisabled();

	@XPath("disabled")
	void setDisabled(boolean disabled);
}
