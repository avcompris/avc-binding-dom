package net.avcompris.binding.dom.polymorphism;

import java.io.File;

import net.avcompris.binding.dom.helper.DomBinderUtils;

public class Xib8PolymorphismTest extends AbstractXibPolymorphismTest {

	@Override
	protected Xib getXib() throws Exception {

		return DomBinderUtils.xmlContentToJava(new File(
				"src/test/xml/006-AVDViewController_iPhone_8_00.xib"),
				Xib8.class);
	}
}
