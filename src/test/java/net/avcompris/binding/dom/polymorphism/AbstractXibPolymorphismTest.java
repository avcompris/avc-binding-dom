package net.avcompris.binding.dom.polymorphism;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.binding.dom.polymorphism.Xib.IBUIButton;
import net.avcompris.binding.dom.polymorphism.Xib.IBUIView;
import net.avcompris.binding.helper.BinderUtils;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractXibPolymorphismTest {

	@Before
	public final void setUp() throws Exception {

		xib = getXib();
	}

	protected abstract Xib getXib() throws Exception;

	private Xib xib;

	@Test
	public final void testUseAutoLayout() throws Exception {

		assertTrue(xib.hasUseAutoLayout());
	}

	@Test
	public final void testUseAutoLayoutNo() throws Exception {

		assertFalse(xib.hasUseAutoLayoutNo());
	}

	@Test
	public final void testGetByTagGetUserLabel() throws Exception {

		final IBUIView button = xib.getIBUIViewByTag(1104);

		assertEquals("LButton 20", button.getUserLabel());
	}

	@Test
	public final void testGetByTagGetFrame() throws Exception {

		final IBUIView button = xib.getIBUIViewByTag(1104);

		assertEquals("{{97, 41}, {128, 127}}", button.getFrame());
	}

	@Test
	public final void testGetByTagGetTag() throws Exception {

		final IBUIView button = xib.getIBUIViewByTag(1104);

		assertEquals(1104, button.getTag());
	}

	@Test
	public final void testGetSubObjectByTag() throws Exception {

		final Xib.IBUIButton button = xib.getIBUIButtonByTag(1104);

		// final DomBinder binder = new DefaultDomBinder();

		assertEquals(1104, button.getTag());

		assertEquals("20", button.getNormalTitle());

		final IBUIView view = xib.getIBUIButtonByTag(1104);

		assertEquals(1104, view.getTag());

		assertEquals("20", ((IBUIButton) view).getNormalTitle());
	}

	@Test
	public final void testRebind0() throws Exception {

		final IBUIView view = xib.getIBUIButtonByTag(1104);

		// final DomBinder binder = new DefaultDomBinder();

		// @SuppressWarnings("unchecked")
		final IBUIButton button =
		// ((Binding<Node>)view).self(IBUIButton.class);
		DomBinderUtils.rebind(view, IBUIButton.class);

		assertEquals(1104, button.getTag());

		assertEquals("20", button.getNormalTitle());
	}

	@Test
	public final void testRebind1() throws Exception {

		final IBUIView view = xib.getIBUIViewByTag(1104);

		final IBUIButton button =
		// ((Binding<Node>)view).self(IBUIButton.class);
		BinderUtils.rebind(view, Xib0.IBUIButton0.class);

		assertEquals(1104, button.getTag());

		assertEquals("20", button.getNormalTitle());
	}
}

interface Xib {

	boolean hasUseAutoLayout();

	boolean hasUseAutoLayoutNo();

	IBUIView getIBUIViewByTag(int tag);

	interface IBUIView {

		String getFrame(); // e.g. "{{82, 77}, {43, 43}}"

		String getUserLabel(); // e.g. "LButton 20"

		int getTag();
	}

	IBUIButton getIBUIButtonByTag(int tag);

	interface IBUIButton extends IBUIView {

		String getNormalTitle(); // e.g. "OK"
	}
}

@XPath("/document[@version = '3.0']")
interface Xib3 extends Xib {

	@Override
	@XPath("@useAutolayout = 'YES'")
	boolean hasUseAutoLayout();

	@Override
	@XPath("@useAutolayout = 'NO'")
	boolean hasUseAutoLayoutNo();

	@Override
	@XPath("//*[@tag = $arg0]")
	IBUIView3 getIBUIViewByTag(int tag);

	interface IBUIView3 extends Xib.IBUIView {

		@Override
		@XPath("concat('{{', rect[@key = 'frame']/@x, ', '"
				+ ", rect[@key = 'frame']/@y, '}, {'"
				+ ", rect[@key = 'frame']/@width, ', '"
				+ ", rect[@key = 'frame']/@height, '}}')")
		String getFrame(); // e.g. "{{82, 77}, {43, 43}}"

		@Override
		@XPath("@userLabel")
		String getUserLabel(); // e.g. "LButton 20"

		@Override
		@XPath("@tag")
		int getTag();
	}

	@Override
	@XPath("//*[@tag = $arg0]")
	IBUIButton3 getIBUIButtonByTag(int tag);

	interface IBUIButton3 extends IBUIView3, IBUIButton {

		@Override
		@XPath("state[@key = 'normal']/@title")
		String getNormalTitle(); // e.g. "OK"
	}
}

@XPath("/archive[@version = '8.00']")
interface Xib8 extends Xib {

	@Override
	@XPath("data/bool[@key = 'IBDocument.UseAutolayout'] = 'YES'")
	boolean hasUseAutoLayout();

	@Override
	@XPath("data/bool[@key = 'IBDocument.UseAutolayout'] = 'NO'")
	boolean hasUseAutoLayoutNo();

	@Override
	@XPath("//object[int[@key = 'IBUITag'] = $arg0]")
	IBUIView8 getIBUIViewByTag(int tag);

	interface IBUIView8 extends Xib.IBUIView {

		@Override
		@XPath("string[@key = 'NSFrame']")
		String getFrame(); // e.g. "{{82, 77}, {43, 43}}"

		@Override
		@XPath("//object[@class = 'IBObjectRecord'"
				+ " and reference[@key = 'object']/" //
				+ "@ref = $this/@id]/" //
				+ "string[@key = 'objectName']")
		String getUserLabel(); // e.g. "LButton 20"

		@Override
		@XPath("int[@key = 'IBUITag']")
		int getTag();
	}

	@Override
	@XPath("//object[int[@key = 'IBUITag'] = $arg0]")
	IBUIButton8 getIBUIButtonByTag(int tag);

	interface IBUIButton8 extends IBUIButton, IBUIView8 {

		@Override
		@XPath("string[@key = 'IBUINormalTitle']")
		String getNormalTitle(); // e.g. "OK"
	}
}

@XPath("/document[@version = '3.0'] | /archive[@version = '8.00']")
interface Xib0 extends Xib {

	@Override
	@XPath("@useAutolayout = 'YES'"
			+ " or data/bool[@key = 'IBDocument.UseAutolayout'] = 'YES'")
	boolean hasUseAutoLayout();

	@Override
	@XPath("@useAutolayout = 'NO'"
			+ " or data/bool[@key = 'IBDocument.UseAutolayout'] = 'NO'")
	boolean hasUseAutoLayoutNo();

	@Override
	@XPath("//*[@tag = $arg0] | //object[int[@key = 'IBUITag'] = $arg0]")
	IBUIView0 getIBUIViewByTag(int tag);

	interface IBUIView0 extends Xib.IBUIView {

		@Override
		@XPath("substring(concat('{{', rect[@key = 'frame']/@x, ', '"
				+ ", rect[@key = 'frame']/@y, '}, {'"
				+ ", rect[@key = 'frame']/@width, ', '"
				+ ", rect[@key = 'frame']/@height, '}}'"
				+ ", string[@key = 'NSFrame']),"
				+ " 13 - 12 * count(rect[@key = 'frame']))")
		String getFrame(); // e.g. "{{82, 77}, {43, 43}}"

		@Override
		@XPath("@userLabel | //object[@class = 'IBObjectRecord'"
				+ " and reference[@key = 'object']/" //
				+ "@ref = $this/@id]/" //
				+ "string[@key = 'objectName']")
		String getUserLabel(); // e.g. "LButton 20"

		@Override
		@XPath("int[@key = 'IBUITag'] | ./@tag")
		int getTag();
	}

	@Override
	@XPath("//object[int[@key = 'IBUITag'] = $arg0] | //*[@tag = $arg0]")
	IBUIButton0 getIBUIButtonByTag(int tag);

	interface IBUIButton0 extends IBUIView0, IBUIButton {

		@Override
		@XPath("string[@key = 'IBUINormalTitle'] | state[@key = 'normal']/@title")
		String getNormalTitle(); // e.g. "OK"
	}
}
