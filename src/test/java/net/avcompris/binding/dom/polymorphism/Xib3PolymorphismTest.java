package net.avcompris.binding.dom.polymorphism;

import java.io.File;

import net.avcompris.binding.dom.helper.DomBinderUtils;

public class Xib3PolymorphismTest extends AbstractXibPolymorphismTest {

	@Override
	protected Xib getXib() throws Exception {

		return DomBinderUtils.xmlContentToJava(new File(
				"src/test/xml/005-AVDViewController_iPhone_3_0.xib"),
				Xib3.class);
	}
}
