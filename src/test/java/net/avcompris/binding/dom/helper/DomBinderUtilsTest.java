package net.avcompris.binding.dom.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import net.avcompris.binding.annotation.XPath;

import org.junit.Test;

public class DomBinderUtilsTest {

	@Test
	public void testSimpleXmlContentToJava() throws Exception {

		final Dummy foo = DomBinderUtils.xmlContentToJava(
				"<foo>Hello World!</foo>", Dummy.class);

		assertEquals("Hello World!", foo.getText());

		assertEquals("foo", foo.getNodeName());
	}

	@Test
	public void testRebindCrash() throws Exception {

		try {

			DomBinderUtils.rebind("", Dummy.class);
			
		} catch (final IllegalArgumentException e) {

			assertEquals(
					"Parameter appears not to be an object bound to a node.",
					e.getMessage());

			return;
		}

		fail("Should have crashed");
	}
}

@XPath("/*")
interface Dummy {

	@XPath(".")
	String getText();

	@XPath("name()")
	String getNodeName();
}