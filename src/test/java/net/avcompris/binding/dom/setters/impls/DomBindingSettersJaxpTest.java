package net.avcompris.binding.dom.setters.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.setters.DomBindingSettersTest;

@DomBinderImplClass(JaxpDomBinder.class)
public class DomBindingSettersJaxpTest extends DomBindingSettersTest {

	// empty class
}
