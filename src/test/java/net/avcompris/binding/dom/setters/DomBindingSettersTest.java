package net.avcompris.binding.dom.setters;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.avcompris.binding.Binding;
import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.binding.dom.impl.DefaultDomBinder;
import net.avcompris.binding.dom.testutil.LoadXmlUtils;

@DomBinderImplClass(DefaultDomBinder.class)
public class DomBindingSettersTest extends AbstractDomBinderTest {

	@Test
	public void testSimpleBinding() throws Exception {

		final Node document = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document, Toto.class);

		// <toto id="aaa"/>

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final Toto toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());
	}

	@Test
	public void testSimpleNonBinding() throws Exception {

		final Node document = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		// <toto id="aaa"/>

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final TotoNonBinding toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());
	}

	@Test
	public void testBindingSimpleSame() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto id='aaa'/>");

		final Toto toto = getBinder().bind(document, Toto.class);

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final Toto toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());

		assertEquals(toto.getClass(), toto2.getClass());

		assertSame(toto, toto2);
	}

	@Test
	public void testBindingNullLong() throws Exception {

		final Node document1 = LoadXmlUtils.loadXml("<toto id='aaa'/>");

		final TotoNullableLong toto1 = getBinder().bind(document1, TotoNullableLong.class);

		assertEquals("aaa", toto1.getId());

		assertEquals(null, toto1.getNope());

		final Node document2 = LoadXmlUtils.loadXml("<toto id='aaa'><toto id='bbb'/></toto>");

		final TotoNullableLong toto2 = getBinder().bind(document2, TotoNullableLong.class);

		assertEquals("aaa", toto2.getId());
		assertEquals("bbb", toto2.getToto().getId());
		assertEquals(null, toto2.getToto().getNope());

		final TotoNullableLongContainer toto3 = DomBinderUtils.xmlContentToJava( //
				"<toto id='aaa'><toto id='bbb'/></toto>", TotoNullableLongContainer.class);

		assertEquals("bbb", toto3.getToto().getId());
		assertEquals(null, toto3.getToto().getNope());

		final TotoNullableLongContainer toto4 = DomBinderUtils.xmlContentToJava( //
				"<toto id='aaa'><toto id='bbb' nope='1239129'/></toto>", TotoNullableLongContainer.class);

		assertEquals("bbb", toto4.getToto().getId());
		assertEquals(1239129L, toto4.getToto().getNope().longValue());
	}

	@Test
	public void testNonBindingSimpleSame() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto id='aaa'/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final TotoNonBinding toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());

		assertEquals(toto.getClass(), toto2.getClass());

		assertSame(toto, toto2);
	}

	@Test
	public void testBindingNewAttribute() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final Toto toto = getBinder().bind(document, Toto.class);

		assertTrue(toto.isNullId());

		assertEquals("", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		assertFalse(toto.isNullId());
	}

	@Test
	public void testNonBindingNewAttribute() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertTrue(toto.isNullId());

		assertEquals("", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		assertFalse(toto.isNullId());
	}

	@Test
	public void testNonBindingNewElementWithAttribute() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertTrue(toto.isNullMyTextRoot());

		assertTrue(toto.isNullMyLabel());

		assertEquals("", toto.getMyLabel());

		toto.setMyLabel("Hello World!");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMyLabel());

		assertEquals("Hello World!", toto.getMyLabel());
	}

	@Test
	public void testNonBindingSetNewElementWithAttribute() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMyLabel("Hello World!");

		assertEquals("Hello World!", toto.getMyLabel());

		toto.setMyLabel("Good Bye");

		assertEquals("Good Bye", toto.getMyLabel());
	}

	@Test
	public void testNonBindingNewElementWithText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertTrue(toto.isNullMyTextRoot());

		assertEquals("", toto.getMyTextRoot());

		toto.setMyTextRoot("Hello World!");

		assertFalse(toto.isNullMyTextRoot());

		assertEquals("Hello World!", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingNewElementNSWithText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto xmlns='http://www.w3.org/2005/Atom'/>");

		final TotoNS toto = getBinder().bind(document, TotoNS.class);

		assertTrue(toto.isNullMyTextRoot());

		assertEquals("", toto.getMyTextRoot());

		toto.setMyTextRoot("Hello World!");

		assertFalse(toto.isNullMyTextRoot());

		assertEquals("Hello World!", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingNewElementNSWithSelector() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto xmlns='http://www.w3.org/2005/Atom'/>");

		final TotoNS toto = getBinder().bind(document, TotoNS.class);

		assertTrue(toto.isNullSelf());

		assertEquals("", toto.getSelf());

		assertEquals(0, toto.sizeOfLinks());

		toto.setSelf("a:b:c:d");

		assertFalse(toto.isNullSelf());

		assertEquals("a:b:c:d", toto.getSelf());

		assertEquals(1, toto.sizeOfLinks());

		toto.setSelf("e:f:g:h");

		assertEquals("e:f:g:h", toto.getSelf());

		assertEquals(1, toto.sizeOfLinks());
	}

	@Test
	public void testNonBindingSetNewElementWithText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMyTextRoot("Hello World!");

		assertEquals("Hello World!", toto.getMyTextRoot());

		toto.setMyTextRoot("Good Bye");

		assertEquals("Good Bye", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingSetNewElementWithText_newDom() throws Exception {

		final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

		document.appendChild(document.createElement("toto"));

		final TotoNonBinding toto = getBinder().bind(document.getDocumentElement(), TotoNonBinding.class);

		toto.setMyTextRoot("Hello World!");

		assertEquals("Hello World!", toto.getMyTextRoot());

		toto.setMyTextRoot("Good Bye");

		assertEquals("Good Bye", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingNewElementWithSubWithText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertTrue(toto.isNullMyTextRoot());

		assertTrue(toto.isNullMySub());

		assertEquals("", toto.getMySub());

		toto.setMySub("Hello World!");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMySub());

		assertEquals("Hello World!", toto.getMySub());

		assertEquals("Hello World!", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingNewElementWithTextAndSub() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMyTextRoot("Hello");

		toto.setMySub("World!");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMySub());

		assertEquals("World!", toto.getMySub());

		assertEquals("HelloWorld!", toto.getMyTextRoot());
	}

	@Test
	public void testSettersReturnInstance() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		assertSame(toto, toto.setMyTextRoot("Hello"));

		assertSame(toto, toto.setMyLabel("World!"));
	}

	@Test
	public void testNonBindingNewElementWithLabelAndText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMyLabel("Hello");

		toto.setMySub("World!");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMySub());

		assertEquals("Hello", toto.getMyLabel());

		assertEquals("World!", toto.getMySub());
	}

	@Test
	public void testNonBindingNewElementWithTextAndLabel() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMySub("World!");

		toto.setMyLabel("Hello");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMySub());

		assertEquals("Hello", toto.getMyLabel());

		assertEquals("World!", toto.getMySub());
	}

	@Test
	@Ignore
	public void testNonBindingNewElementWithSubAndText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMySub("World!");

		toto.setMyTextRoot("Hello");

		assertFalse(toto.isNullMyTextRoot());

		assertFalse(toto.isNullMySub());

		assertEquals("World!", toto.getMySub());

		assertEquals("HelloWorld!", toto.getMyTextRoot());
	}

	@Test
	public void testNonBindingSetNewElementWithSubWithText() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto/>");

		final TotoNonBinding toto = getBinder().bind(document, TotoNonBinding.class);

		toto.setMySub("Hello World!");

		assertEquals("Hello World!", toto.getMySub());

		toto.setMySub("Good Bye");

		assertEquals("Good Bye", toto.getMySub());
	}

	@XPath("/toto")
	private interface Toto extends Binding<Toto> {

		@XPath("@id")
		String getId();

		void setId(
			String id
		);

		boolean isNullId();

		@XPath("@id")
		Toto setMyId(
			String id
		);

		@XPath("concat('[toto:id=', @id, ']')")
		@Override
		String toString();

		@XPath("descendant::line")
		int sizeOfLines();

		Line[] getLines();

		interface Line {

			@XPath(".")
			String getText();

			Node toNode();
		}

		@XPath("descendant::line[1]")
		void setFirstLineNode(
			Node node
		);

		@XPath("descendant::line[2]")
		void setSecondLineNode(
			Node node
		);
	}

	@XPath("/toto")
	private interface TotoNullableLong {

		@XPath("@id")
		String getId();

		@XPath("@nope")
		@Nullable
		Long getNope();

		@XPath("toto")
		@Nullable
		TotoNullableLong getToto();
	}

	@XPath("/toto")
	private interface TotoNullableLongContainer {

		@XPath("toto")
		TotoNullableLong getToto();
	}

	@XPath("/toto")
	private interface TotoNonBinding {

		@XPath("@id")
		String getId();

		void setId(
			String id
		);

		boolean isNullId();

		@XPath("@id")
		TotoNonBinding setMyId(
			String id
		);

		@XPath("concat('[toto:id=', @id, ']')")
		@Override
		String toString();

		@XPath("my/@label")
		String getMyLabel();

		TotoNonBinding setMyLabel(
			String text
		);

		boolean isNullMyLabel();

		@XPath("my")
		boolean isNullMyTextRoot();

		String getMyTextRoot();

		Object setMyTextRoot(
			String text
		);

		@XPath("my/sub")
		String getMySub();

		void setMySub(
			String text
		);

		boolean isNullMySub();
	}

	@Namespaces("xmlns:atom=http://www.w3.org/2005/Atom")
	@XPath("/atom:toto")
	private interface TotoNS {

		@XPath("atom:my")
		boolean isNullMyTextRoot();

		String getMyTextRoot();

		Object setMyTextRoot(
			String text
		);

		@XPath("atom:link[@rel = 'self']/@href")
		String getSelf();

		TotoNS setSelf(
			String self
		);

		boolean isNullSelf();

		@XPath("atom:link")
		int sizeOfLinks();
	}

	@Test
	public void testClearEverything() throws Exception {

		final Node document = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document, Toto.class);

		// <toto id="aaa">
		// <line>Hello</line>
		// This text here
		// <line>World!</line>
		// </toto>

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		toto.clear();

		assertTrue(toto.isNullId());
	}

	@Test
	public void testSetSimpleNode0() throws Exception {

		final Node document0 = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());
		final Node document1 = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document0, Toto.class);

		// <toto id="aaa">
		// <line>Hello</line>
		// This text here
		// <line>World!</line>
		// </toto>

		assertEquals(2, toto.sizeOfLines());

		assertEquals("Hello", toto.getLines()[0].getText());
		assertEquals("World!", toto.getLines()[1].getText());

		toto.setFirstLineNode(document1);

		assertEquals(3, toto.sizeOfLines());

		assertEquals("Hello", toto.getLines()[0].getText());
		assertEquals("World!", toto.getLines()[1].getText());
		assertEquals("World!", toto.getLines()[2].getText());

		// assertNotSame(document1, toto.getLines()[0].toNode()); // TODO
		// toNode()
	}

	@Test
	public void testSetSimpleNode1() throws Exception {

		final Node document0 = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());
		final Node document1 = LoadXmlUtils.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document0, Toto.class);

		// <toto id="aaa">
		// <line>Hello</line>
		// This text here
		// <line>World!</line>
		// </toto>

		assertEquals(2, toto.sizeOfLines());

		assertEquals("Hello", toto.getLines()[0].getText());
		assertEquals("World!", toto.getLines()[1].getText());

		toto.setSecondLineNode(document1);

		assertEquals(3, toto.sizeOfLines());

		assertEquals("Hello", toto.getLines()[0].getText());
		assertEquals("Hello", toto.getLines()[1].getText());
		assertEquals("World!", toto.getLines()[2].getText());

		// assertNotSame(document1, toto.getLines()[0].toNode()); // TODO
		// toNode()
	}

	@Test
	public void test_setAttributesJasperReport() throws Exception {

		final Node node = loadXml("src/test/xml/004-jasperReport.xml");

		final MyJasperReport jr = new DefaultDomBinder().bind(node, MyJasperReport.class);

		assertEquals(2, jr.getColumnCount());
		jr.setColumnCount(4);
		assertEquals(4, jr.getColumnCount());

		assertEquals(520, jr.getPageWidth());
		jr.setPageWidth(1024);
		assertEquals(1024, jr.getPageWidth());

		assertEquals("1.0", jr.getZoom());
		jr.setZoom("1.45");
		assertEquals("1.45", jr.getZoom());
	}

	@Test
	public void test_setSubElement() throws Exception {

		final Node node = loadXml("src/test/xml/004-jasperReport.xml");

		final MyJasperReport jr = new DefaultDomBinder().bind(node, MyJasperReport.class);

		jr.setNewSubField("Hello World!");

		assertEquals("Hello World!", jr.getNewSubField());
	}

	@Test
	public void test_setSubAttribute() throws Exception {

		final Node node = loadXml("src/test/xml/004-jasperReport.xml");

		final MyJasperReport jr = new DefaultDomBinder().bind(node, MyJasperReport.class);

		jr.setNewSubAttribute("12345");

		assertEquals("12345", jr.getNewSubAttribute());
	}

	@Test
	public void test_setSubAttributeInt() throws Exception {

		final Node node = loadXml("src/test/xml/004-jasperReport.xml");

		final MyJasperReport jr = new DefaultDomBinder().bind(node, MyJasperReport.class);

		jr.setNewSubAttributeInt(12345);

		assertEquals(12345, jr.getNewSubAttributeInt());
	}
}

/**
 * Illustration of an answer in Stack Overflow: <a href=
 * "http://stackoverflow.com/questions/13283683/how-i-can-modify-xml-field-value-by-sax-parser"
 * >"how i can modify xml field value by sax parser"</a>
 */
@Namespaces("xmlns:jr=http://jasperreports.sourceforge.net/jasperreports")
@XPath("/jr:jasperReport")
interface MyJasperReport {

	@XPath("@columnCount")
	int getColumnCount();

	void setColumnCount(
		int columnCount
	);

	@XPath("@pageWidth")
	int getPageWidth();

	void setPageWidth(
		int pageWidth
	);

	@XPath("@pageHeight")
	int getPageHeight();

	void setPageHeight(
		int pageHeight
	);

	@XPath("jr:property[@name = 'ireport.zoom']/@value")
	String getZoom();

	void setZoom(
		String zoom
	);

	@XPath("jr:detail/jr:newField/jr:newSubField")
	MyJasperReport setNewSubField(
		String value
	);

	String getNewSubField();

	@XPath("jr:detail/jr:newField/@newAttribute")
	MyJasperReport setNewSubAttribute(
		String value
	);

	String getNewSubAttribute();

	@XPath("jr:detail/jr:newField/@newAttribute")
	MyJasperReport setNewSubAttributeInt(
		int value
	);

	int getNewSubAttributeInt();

	/*
	 * <jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports"
	 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:
	 * schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd"
	 * name="Hello_subreport1_subreport1" language="groovy" columnCount="2"
	 * printOrder="Horizontal" pageWidth="520" pageHeight="802" columnWidth="260"
	 * leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0"
	 * uuid="ac19d62f-eac8-428e-8e0a-9011534189ed"> <property name="ireport.zoom"
	 * value="1.0"/> <property name="ireport.x" value="0"/> <property
	 * name="ireport.y" value="0"/> <queryString> <![CDATA[]]> </queryString> <field
	 * name="subjectName" class="java.lang.String">
	 * <fieldDescription><![CDATA[subjectName]]></fieldDescription> </field> <field
	 * name="subjectID" class="java.lang.Integer">
	 * <fieldDescription><![CDATA[subjectID]]></fieldDescription> </field> <field
	 * name="maxMarks" class="java.lang.Integer">
	 * <fieldDescription><![CDATA[maxMarks]]></fieldDescription> </field> <field
	 * name="redMarks" class="java.lang.Float">
	 * <fieldDescription><![CDATA[redMarks]]></fieldDescription> </field> <field
	 * name="passMarks" class="java.lang.Integer">
	 * <fieldDescription><![CDATA[passMarks]]></fieldDescription> </field> <field
	 * name="marks" class="java.lang.Float">
	 * <fieldDescription><![CDATA[marks]]></fieldDescription> </field> <background>
	 * <band splitType="Stretch"/> </background> <detail>
	 */
}