package net.avcompris.binding.dom.setters.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.setters.DomBindingSettersTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class DomBindingSettersJaxenTest extends DomBindingSettersTest {

	// empty class
}
