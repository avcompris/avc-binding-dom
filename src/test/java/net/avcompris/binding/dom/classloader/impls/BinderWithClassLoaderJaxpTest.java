package net.avcompris.binding.dom.classloader.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.classloader.BinderWithClassLoaderTest;
import net.avcompris.binding.dom.impl.JaxpDomBinder;

@DomBinderImplClass(JaxpDomBinder.class)
public class BinderWithClassLoaderJaxpTest extends BinderWithClassLoaderTest {

	// empty class
}
