package net.avcompris.binding.dom.classloader.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.classloader.BinderWithClassLoaderTest;
import net.avcompris.binding.dom.impl.JaxenDomBinder;

@DomBinderImplClass(JaxenDomBinder.class)
public class BinderWithClassLoaderJaxenTest extends BinderWithClassLoaderTest {

	// empty class
}
