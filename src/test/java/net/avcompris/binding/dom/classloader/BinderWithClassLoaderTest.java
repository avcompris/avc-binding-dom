package net.avcompris.binding.dom.classloader;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import groovy.lang.GroovyClassLoader;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;

@DomBinderImplClass(DefaultDomBinder.class)
public class BinderWithClassLoaderTest extends AbstractDomBinderTest {

	private static final String TUTU_GROOVY_CODE = "package tutu\r\n" + //
			"import " + XPath.class.getCanonicalName() + "\r\n" + //
			"@XPath('/toto') public interface Tutu" + //
			"{ @XPath(value = '.', function = 'count(.)') int getOne() }";

	@Test(expected = ClassNotFoundException.class)
	public final void testClassLoadersExplicitClassNotFound() throws Exception {

		final ClassLoader baseClassLoader = XPath.class.getClassLoader();

		final GroovyClassLoader groovyClassLoader = new GroovyClassLoader(
				Thread.currentThread().getContextClassLoader());

		final Class<?> groovyClass = groovyClassLoader
				.parseClass(TUTU_GROOVY_CODE);

		assertEquals("tutu.Tutu", groovyClass.getCanonicalName());

		Class.forName("tutu.Tutu", false, baseClassLoader);
	}

	@Test
	public final void testClassLoadersExplicitClassFound() throws Exception {

		final GroovyClassLoader groovyClassLoader = new GroovyClassLoader(
				Thread.currentThread().getContextClassLoader());

		final Class<?> groovyClass = groovyClassLoader
				.parseClass(TUTU_GROOVY_CODE);

		assertEquals("tutu.Tutu", groovyClass.getCanonicalName());

		Class.forName("tutu.Tutu", false, groovyClassLoader);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void testBadExplicitClassLoader() throws Exception {

		final ClassLoader baseClassLoader = XPath.class.getClassLoader();

		final GroovyClassLoader groovyClassLoader = new GroovyClassLoader(
				Thread.currentThread().getContextClassLoader());

		final Class<?> groovyClass = groovyClassLoader
				.parseClass(TUTU_GROOVY_CODE);

		final ClassLoader badClassLoader = baseClassLoader;

		assertEquals("tutu.Tutu", groovyClass.getName());

		getBinder().bind(loadXml("<toto/>"), badClassLoader, groovyClass);
	}

	@Test
	public final void testGoodDefaultClassLoader() throws Exception {

		final Toto toto = getBinder().bind(loadXml("<toto/>"), Toto.class);

		assertEquals(1, toto.getOne());
	}

	@Test
	public final void testGoodExplicitClassLoader() throws Exception {

		final Toto toto = getBinder().bind(loadXml("<toto/>"),
				Toto.class.getClassLoader(), Toto.class);

		assertEquals(1, toto.getOne());

		final Toto toto2 = getBinder().bind(loadXml("<toto/>"),
				XPath.class.getClassLoader(), Toto.class);

		assertEquals(1, toto2.getOne());
	}

	@Test
	public final void testGoodCurrentThreadClassLoader() throws Exception {

		final Toto toto = getBinder().bind(loadXml("<toto/>"),
				Thread.currentThread().getContextClassLoader(), Toto.class);

		assertEquals(1, toto.getOne());
	}
}
