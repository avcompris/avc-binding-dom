package net.avcompris.binding.dom.classloader;

import net.avcompris.binding.annotation.XPath;

@XPath("/toto")
public interface Toto {

	@XPath(value=".", function="count(.)")
	int getOne();
}
