package net.avcompris.binding.dom.avcProjects;

import net.avcompris.binding.annotation.XPath;

@XPath("/avc-workspace")
public interface AvcProjectsInfo2 extends AvcProjectsInfo {

	@Override
	@XPath(value = "distributionManagement/repository[id = 'avcompris']/url", function = "normalize-space()")
	String getDistributionMgtReleasesUrl();

	@Override
	boolean isNullDistributionMgtReleasesUrl();
}
