package net.avcompris.binding.dom.avcProjects;

import net.avcompris.binding.annotation.XPath;

public interface AvcProjectsInfoNoRootXPath {

	@XPath("name()")
	String getRootName();
}
