package net.avcompris.binding.dom.avcProjects;

import static com.avcompris.util.junit.AvcMatchers.isIn;
import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.avcProjects.AvcProjectsInfo.HgRepository;
import net.avcompris.binding.dom.avcProjects.AvcProjectsInfo.Repository;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

@DomBinderImplClass(DefaultDomBinder.class)
public class AvcProjectsTest extends AbstractDomBinderTest {

	@Test
	public void test_loadInnerInterfaces_and_attributes() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals("hg", a.getRepositories()[0].getType());
		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositories()[0].getUrl());
		assertEquals("trunk", a.getRepositories()[0].getBranch());

		assertEquals("hg", a.getRepositories()[2].getType());
		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepositories()[2].getUrl());
		assertEquals("default", a.getRepositories()[2].getBranch());

		assertThat("avc-base-parent",
				isIn("getName", a.getRepositories()[0].getProjects()));
		assertEquals("true", a.getRepositories()[0].getProjects()[0].getValue());

		assertThat("avc-system",
				isIn("getName", a.getRepositories()[3].getProjects()));
		assertEquals("false",
				a.getRepositories()[3].getProjectByName("avc-system")
						.getValue());
	}

	@Test
	public void test_loadCollectionOfStrings() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		final AvcProjectsInfo.Repository r = a.getRepositories()[0];

		final Collection<String> c = r.getProjectNamesAsCollection();

		assertEquals(6, c.size());

		assertTrue(c.contains("avc-commons-testutil"));
		assertTrue(c.contains("avc-commons-lang"));

		assertEquals(6, new HashSet<String>(c).size());
	}

	@Test
	public void test_sizeOf() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.sizeOfRepositories());
	}

	@Test
	public void test_isNotNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertFalse(a.isNullRepositories());

		assertNotNull(a.getRepositories());

		assertFalse(a.getRepositories()[0].isNullBranch());

		assertEquals("hg", a.getRepositories()[0].getType());
	}

	@Test
	public void test_isNullNodeInSubclass() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.isNullDistributionMgtReleasesUrl());

		final AvcProjectsInfo2 a2 = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo2.class);

		assertTrue(a2.isNullDistributionMgtReleasesUrl());
	}

	@Test
	public void test_isNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.isNullRepositories());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_crashGetNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		a.getRepositories();
	}

	@Test
	public void test_getNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/*/hg[2]").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertNotNull(a.getRepositories());

		assertEquals(0, a.getRepositories().length);
	}

	@Test
	public void test_getIsNotNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.hasRepositories());

		assertFalse(a.getMyIsNullRepositories());
	}

	@Test
	public void test_has_directCount() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.hasRepositoriesDirectCount());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_crashGetIsNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		a.getMyIsNullRepositories();
	}

	@Test
	public void test_sizeOfNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(0, a.sizeOfRepositories());
	}

	@Test
	public void test_getMyIsNullOfNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.getMyIsNullXxx());
	}

	@Test
	public void test_getMyIsNullOfNotNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.getMyIsNullXxx());
	}

	@Test
	public void test_getMySizeOfNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(0, a.getMySizeOfXxx());
	}

	@Test
	public void test_getMySizeOfNotNull() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(0, a.getMySizeOfXxx());
	}

	@Test
	public void test_hasIsNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertFalse(a.hasRepositories());
	}

	@Test
	public void test_hasIsNullArrayDirectCount() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/*/*").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertFalse(a.hasRepositoriesDirectCount());
	}

	@Test
	public void test_getIsNullArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/*/hg[2]").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertFalse(a.hasRepositories());

		assertTrue(a.getMyIsNullRepositories());
	}

	@Test
	public void test_getSizeOfArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.getMySizeOfRepositories());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_getSizeOfZero() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		a.getMySizeOfRepositories();
	}

	@Test
	public void test_getAlternateXPathExpression() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.getRepositories().length);

		assertEquals(5, a.getRepositoriesAlternateXPathExpression().length);
	}

	@Test
	public void test_getSimpleArray() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.getRepositories().length);
	}

	@Test
	public void test_getSimpleSubObject() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		final Repository firstRepository = a.getFirstRepository();

		assertEquals("trunk", firstRepository.getBranch());
	}

	@Test
	public void test_isNullField() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.getRepositories()[4].isNullBranch());

		assertEquals("", a.getRepositories()[4].getBranch());

		assertEquals("hg", a.getRepositories()[4].getType());
	}

	@Test
	public void test_nullableField() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.getRepositories()[4].isNullBranch());

		assertTrue(a.getRepositories()[4].isNullBranchNullable());

		assertNotNull(a.getRepositories()[4].getBranch());

		assertNull(a.getRepositories()[4].getBranchNullable());

		assertNull(a.getRepositories()[4].getBranchJavaxNullable());
	}

	@Test
	public void test_nullableObjectField() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertNull(a.getNullRepository());
	}

	@Test
	public void test_nullableDateTimeField() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertNull(a.getNullDateTime());
	}
	
	@Test(expected=NullPointerException.class)
	public void test_notNullableObjectField() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		final Repository repository = a.getFirstRepository();
		
		repository.getProjectByName("toto");
	}

	@Test
	public void test_hasSuchNode() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertTrue(a.hasRepositories());
	}

	@Test
	public void test_hasNotSuchNode() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				BindConfiguration.newBuilder("/toto").build(),
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertFalse(a.hasRepositories());
	}

	@Test
	public void test_getMap() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		final Map<String, Repository> map = a.getRepositoryMap();

		assertEquals(5, map.size());

		assertEquals(
				25,
				map.get("ssh://kimsufi//home/hg/projects/${avc.project}::default")
						.sizeOfProjects());
	}

	@Test
	public void testAddToRepositories() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.sizeOfRepositories());

		final Repository r = a.addToRepositories();

		assertEquals("toto", r.name());

		assertEquals(5, a.sizeOfRepositories());

		r.rename("hg");

		assertEquals("hg", r.name());

		assertEquals(6, a.sizeOfRepositories());
	}

	@Test
	public void testAddToEmptyRepositories() throws Exception {

		final AvcProjectsInfo a = getBinder().bind(loadXml("<avc-workspace/>"),
				AvcProjectsInfo.class);

		assertEquals(0, a.sizeOfRepositories());

		final HgRepository r = a.addToHgRepositories();

		 assertEquals("hg", r.getName());

		assertEquals(1, a.sizeOfRepositories());
	}

	@Test
	public void testNoRootXPath() throws Exception {

		final AvcProjectsInfoNoRootXPath a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfoNoRootXPath.class);

		assertEquals("avc-workspace", a.getRootName());
	}

	@Test
	public void testDocumentRootXPath() throws Exception {

		final AvcProjectsInfoDocumentRootXPath a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsInfoDocumentRootXPath.class);

		assertEquals("", a.getRootName());

		assertEquals(1, a.sizeOfChildNames());

		assertEquals("avc-workspace", a.getChildNames()[0]);
	}
}
