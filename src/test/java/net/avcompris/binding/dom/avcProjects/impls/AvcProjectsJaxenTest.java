package net.avcompris.binding.dom.avcProjects.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.avcProjects.AvcProjectsTest;
import net.avcompris.binding.dom.impl.JaxenDomBinder;

@DomBinderImplClass(JaxenDomBinder.class)
public class AvcProjectsJaxenTest extends AvcProjectsTest {

	// empty class
}
