package net.avcompris.binding.dom.avcProjects;

import net.avcompris.binding.annotation.XPath;

@XPath("/")
public interface AvcProjectsInfoDocumentRootXPath {

	@XPath("name()")
	String getRootName();

	@XPath(value = "*", function = "name()")
	String[] getChildNames();

	int sizeOfChildNames();
}
