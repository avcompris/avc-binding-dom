package net.avcompris.binding.dom.avcProjects;

import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import com.avcompris.common.annotation.Nullable;

import net.avcompris.binding.Binding;
import net.avcompris.binding.annotation.XPath;

@XPath("/avc-workspace")
public interface AvcProjectsInfo {

	@XPath("hg | svn")
	Repository[] getRepositories();

	Repository addToRepositories();

	@XPath("hg[1]")
	Repository getFirstRepository();

	@XPath("hg[toto]")
	@Nullable
	Repository getNullRepository();

	@XPath("inexistent")
	@Nullable
	DateTime getNullDateTime();

	@XPath("svn | hg")
	Repository[] getRepositoriesAlternateXPathExpression();

	int sizeOfRepositories();

	@XPath("toto")
	interface Repository extends Binding<Repository> {

		@XPath(value = ".", function = "name()")
		String getType();

		@XPath("@repository")
		String getUrl();

		@XPath("@branch")
		String getBranch();

		@XPath("@branch")
		@Nullable
		String getBranchNullable();

		@XPath("@branch")
		@javax.annotation.Nullable
		String getBranchJavaxNullable();

		@XPath("avc-projects/@*")
		AvcProject[] getProjects();

		@XPath("avc-projects/@*[name() = $arg0]")
		AvcProject getProjectByName(String name);

		@XPath(value = "avc-projects/@*", function = "name()", collectionComponentType = String.class)
		List<String> getProjectNamesAsCollection();

		int sizeOfProjects();

		boolean isNullBranch();

		boolean isNullBranchNullable();
	}

	@XPath("hg")
	Repository[] getHgRepositories();

	HgRepository addToHgRepositories();

	interface HgRepository {

		@XPath("name()")
		String getName();
	}

	@XPath("ignore/avc-projects/@*")
	AvcProject[] getIgnoredProjects();

	interface AvcProject {

		@XPath(value = ".", function = "name()")
		String getName();

		@XPath(".")
		String getValue();
	}

	@XPath(value = "distributionManagement/repository[id = 'avcompris']/url", function = "normalize-space()")
	String getDistributionMgtReleasesUrl();

	boolean isNullDistributionMgtReleasesUrl();

	boolean isNullRepositories();

	@XPath(value = "hg | svn", function = "count(.) != 0", failFunction = "false()")
	boolean hasRepositories();

	@XPath(value = "hg | svn", mapKeysXPath = ".", mapKeysFunction = "concat(@repository, '::', @branch)", mapValuesType = Repository.class)
	Map<String, Repository> getRepositoryMap();

	@XPath(value = ".", function = "count(hg | svn) = 0")
	boolean getMyIsNullRepositories();

	@XPath(value = "count(hg | svn)")
	int getMySizeOfRepositories();

	@XPath(value = "count(hg | svn) != 0")
	boolean hasRepositoriesDirectCount();

	@XPath(value = "count(xxx) = 0", failFunction = "true()")
	boolean getMyIsNullXxx();

	@XPath(value = "count(xxx)", failFunction = "0")
	int getMySizeOfXxx();
}
