package net.avcompris.binding.dom.avcProjects.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.avcProjects.AvcProjectsTest;
import net.avcompris.binding.dom.impl.JaxpDomBinder;

@DomBinderImplClass(JaxpDomBinder.class)
public class AvcProjectsJaxpTest extends AvcProjectsTest {

	// empty class
}
