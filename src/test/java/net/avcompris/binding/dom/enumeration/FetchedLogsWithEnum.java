package net.avcompris.binding.dom.enumeration;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.XPath;

/**
 * this interface represents log lines fetched via the Adm Web Service.
 * 
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
public interface FetchedLogsWithEnum {

	@XPath("requests/request/log")
	FetchedLogWithEnum[] getLogs();

	interface FetchedLogWithEnum {

		@XPath("@range")
		int getRange();

		@XPath("@level")
		Level getLevel();

		/**
		 * Non-existent field, to test nullable enums.
		 */
		@XPath("@X-level")
		@Nullable
		Level getXLevel();
	}

	enum Level {

		D, I, W, E, F;
	}
}
