package net.avcompris.binding.dom.enumeration;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import net.avcompris.binding.dom.enumeration.FetchedLogsWithEnum.Level;
import net.avcompris.binding.dom.helper.DomBinderUtils;

public class EnumTest {

	@Test
	public void testEnum() throws IOException {

		final FetchedLogsWithEnum logs = DomBinderUtils.xmlContentToJava(
				new File("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithEnum.class);

		assertSame(Level.I,logs.getLogs()[0].getLevel());
	}

	@Test
	public void testNullEnum() throws IOException {

		final FetchedLogsWithEnum logs = DomBinderUtils.xmlContentToJava(
				new File("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithEnum.class);

		assertNull(logs.getLogs()[0].getXLevel());
	}
}
