package net.avcompris.binding.dom.variables;

import net.avcompris.binding.annotation.XPath;

@XPath("/avc-workspace")
public interface AvcProjectsWithVariables {

	@XPath("count(*[name() = substring-after($methodName, '_')])")
	int getRepositoryCount_hg();

	@XPath("hg[@repository = $arg0]")
	Repository getRepository(String url);

	@XPath("hg[count(preceding-sibling::hg) = $arg0]")
	Repository getRepository(int index);

	interface Repository {

		@XPath("@repository")
		String getUrl();

		@XPath("@*[name() = substring-after($methodName, '_')]")
		String get_branch();
	}

	@XPath("hg[@repository = $arg0]/@repository")
	String getRepositoryUrl(String url);

	@XPath("hg[@repository = $0]/@repository")
	String getRepositoryUrlArgZero(String url);

	@XPath("hg[count(preceding-sibling::hg) = $arg0]/@repository")
	String getRepositoryUrl(int index);

	@XPath("hg[count(preceding-sibling::hg) = $arg0]/@repository")
	String getRepositoryUrlWithIndexAsString(String index);

	@XPath("hg[count(preceding-sibling::hg) = $arg0_my_pos]/@repository")
	String getRepositoryUrl_underscore_symbol(int index);

	@XPath("hg[count(preceding-sibling::hg) = $arg0-my-pos]/@repository")
	String getRepositoryUrl_dash_symbol(int index);

	@XPath("hg[count(preceding-sibling::hg) = $arg0mypos]/@repository")
	String getRepositoryUrl_letter_symbol(int index);

	@XPath("//hg[count(preceding-sibling::hg) = $0]/@repository")
	String getRepositoryUrlArgZero(int index);

	@XPath("//hg[count(preceding-sibling::hg) = $0_myPos]/@repository")
	String getRepositoryUrlArgZero_underscore_symbol(int index);

	@XPath("//hg[count(preceding-sibling::hg) = $0-myPos]/@repository")
	String getRepositoryUrlArgZero_dash_symbol(int index);

	@XPath("//hg[count(preceding-sibling::hg) = $0myPos]/@repository")
	String getRepositoryUrlArgZero_letter_symbol(int index);

	@XPath("hg[starts-with(@repository, $arg0)]")
	Repository[] getRepositoriesStartingWith(String prefix);
}
