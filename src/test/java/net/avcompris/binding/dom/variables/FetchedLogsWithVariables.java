package net.avcompris.binding.dom.variables;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.logs.FetchedRequest;

@XPath("/logs")
public interface FetchedLogsWithVariables {

	@XPath("requests/request[@id = $arg0]")
	FetchedRequest getRequestById(String id);

	FetchedRequest getRequestById(int id);
	
	@XPath("requests/request[@id = $arg0]/log[@range = $arg1]")
	LogWithVariables getLogByPosition(int requestId, int range);
	
	interface LogWithVariables {
		
		@XPath("parent::request/@id")
		int getRequestId();
		
		@XPath("@range")
		int getRange();
		
		@XPath("@msgTruncated")
		String getMsgTruncated();
		
		@XPath("//log[@msgTruncated = $this/@msgTruncated]")
		LogWithVariables[] getLogsWithSameMsgTruncated();
		
		int sizeOfWithSameMsgTruncated();
	}
}
