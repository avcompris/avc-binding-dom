package net.avcompris.binding.dom.variables;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import net.avcompris.binding.dom.AbstractDomBinderTest;

import org.junit.Test;

public abstract class AbstractAvcProjectsWithVariablesTest extends
		AbstractDomBinderTest {

	@Test
	public final void test_methodName() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals(5, a.getRepositoryCount_hg());
	}

	@Test
	public final void test_arg0isString_sub() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertNotNull("https://${avc.project}.googlecode.com/hg",
				a.getRepository("https://${avc.project}.googlecode.com/hg"));

		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepository("https://${avc.project}.googlecode.com/hg")
				.getUrl());

		assertEquals(
				"ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepository(
						"ssh://kimsufi//home/hg/projects/${avc.project}")
						.getUrl());
	}

	@Test
	public final void test_arg0isInt() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositoryUrlWithIndexAsString("0"));
		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositoryUrl(1));
		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositoryUrl(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepositoryUrl(2));
	}

	@Test
	public final void test_arg0_symbols_isInt() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositoryUrl_dash_symbol(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepositoryUrl_underscore_symbol(2));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepositoryUrl_letter_symbol(2));
	}

	@Test
	public final void test_arg0isString() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg",
				a.getRepositoryUrl("https://${avc.project}.googlecode.com/hg"));

		assertEquals(
				"ssh://kimsufi//home/hg/projects/${avc.project}",
				a.getRepositoryUrl("ssh://kimsufi//home/hg/projects/${avc.project}"));
	}

	@Test
	public final void test_arg0isArray() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals(3, a.getRepositoriesStartingWith("ssh://").length);

		assertEquals(2, a.getRepositoriesStartingWith("https://").length);
	}
}
