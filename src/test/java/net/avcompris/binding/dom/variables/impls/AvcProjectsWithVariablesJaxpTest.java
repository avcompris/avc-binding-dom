package net.avcompris.binding.dom.variables.impls;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.variables.AbstractAvcProjectsWithVariablesTest;
import net.avcompris.binding.dom.variables.AvcProjectsWithVariables;

import org.junit.Test;

@DomBinderImplClass(JaxpDomBinder.class)
public class AvcProjectsWithVariablesJaxpTest extends
		AbstractAvcProjectsWithVariablesTest {

	@Test
	public void test_methodName_sub() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("trunk", a.getRepository(0).get_branch());

		assertEquals("default", a.getRepository(2).get_branch());
	}

	@Test
	public void test_arg0isInt_sub() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepository(0).getUrl());

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepository(2).getUrl());
	}

	@Test
	public void test_0isInt() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepositoryUrlArgZero(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrlArgZero(2));
	}

	@Test
	public void test_0_symbols_isInt() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("", a.getRepositoryUrlArgZero_dash_symbol(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrlArgZero_underscore_symbol(2));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrlArgZero_letter_symbol(2));
	}

	@Test
	public final void test_arg0isInt_Jaxp() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepositoryUrl(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrl(2));
	}

	@Test
	public final void test_arg0_symbols_isInt_Jaxp() throws Exception {

		final AvcProjectsWithVariables a = getBinder().bind(
				loadXml("src/test/xml/003-avc-projects.xml"),
				AvcProjectsWithVariables.class);

		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepositoryUrl_dash_symbol(0));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrl_underscore_symbol(2));

		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositoryUrl_letter_symbol(2));
	}
}
