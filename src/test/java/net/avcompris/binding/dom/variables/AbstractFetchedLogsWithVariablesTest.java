package net.avcompris.binding.dom.variables;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.dom.impl.DomUniqueBinder;
import net.avcompris.binding.dom.logs.FetchedRequest;
import net.avcompris.binding.dom.variables.FetchedLogsWithVariables.LogWithVariables;

import org.junit.Test;

public abstract class AbstractFetchedLogsWithVariablesTest extends
		AbstractDomBinderTest {

	@Test
	public final void testGetByIdAsStringAndAsInt() throws Exception {

		final DomBinder binder = new DomUniqueBinder(getBinder());

		final FetchedLogsWithVariables logs = binder.bind(
				loadXml("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithVariables.class);

		final FetchedRequest request0 = logs.getRequestById("5");
		final FetchedRequest request1 = logs.getRequestById(5);

		assertSame(request0, request1);

		assertEquals(5, request0.getId());
	}

	@Test
	public final void testThisAsXPathVariable() throws Exception {

		final DomBinder binder = new DomUniqueBinder(getBinder());

		final FetchedLogsWithVariables logs = binder.bind(
				loadXml("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithVariables.class);

		final LogWithVariables log = logs.getLogByPosition(7, 5);

		assertEquals(7, log.getRequestId());
		assertEquals(5, log.getRange());

		final LogWithVariables[] gets = log.getLogsWithSameMsgTruncated();

		assertEquals(6, gets.length);
		
		assertEquals("REQUEST_METHOD: GET", gets[0].getMsgTruncated());
	}
}
