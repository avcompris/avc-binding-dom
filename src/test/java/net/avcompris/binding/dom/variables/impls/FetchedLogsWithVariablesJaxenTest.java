package net.avcompris.binding.dom.variables.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.variables.AbstractFetchedLogsWithVariablesTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class FetchedLogsWithVariablesJaxenTest extends
		AbstractFetchedLogsWithVariablesTest {

	// empty class
}
