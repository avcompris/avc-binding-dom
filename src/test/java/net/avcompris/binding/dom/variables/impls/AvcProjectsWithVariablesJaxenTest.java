package net.avcompris.binding.dom.variables.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.variables.AbstractAvcProjectsWithVariablesTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class AvcProjectsWithVariablesJaxenTest extends
		AbstractAvcProjectsWithVariablesTest {

	// empty class
}
