package net.avcompris.binding.dom.variables;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

@DomBinderImplClass(DefaultDomBinder.class)
public class AvcProjectsWithVariablesTest extends
		AbstractAvcProjectsWithVariablesTest {

	// empty class
}
