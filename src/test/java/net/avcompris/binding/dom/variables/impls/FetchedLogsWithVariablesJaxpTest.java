package net.avcompris.binding.dom.variables.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.variables.AbstractFetchedLogsWithVariablesTest;

@DomBinderImplClass(JaxpDomBinder.class)
public class FetchedLogsWithVariablesJaxpTest extends
		AbstractFetchedLogsWithVariablesTest {

	// empty class
}
