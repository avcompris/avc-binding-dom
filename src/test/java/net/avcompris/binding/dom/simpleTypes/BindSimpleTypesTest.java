package net.avcompris.binding.dom.simpleTypes;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;

@DomBinderImplClass(DefaultDomBinder.class)
public class BindSimpleTypesTest extends AbstractDomBinderTest {

	@Test
	public void testBindString() throws Exception {

		final String s = getBinder().bind(
				BindConfiguration.newBuilder("/avc-workspace/hg/@repository")
						.build(), loadXml("src/test/xml/003-avc-projects.xml"),
				String.class);

		assertEquals("https://${avc.project}.googlecode.com/hg", s);
	}
}
