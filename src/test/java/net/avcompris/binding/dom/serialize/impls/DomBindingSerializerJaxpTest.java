package net.avcompris.binding.dom.serialize.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.serialize.DomBindingSerializerTest;

@DomBinderImplClass(JaxpDomBinder.class)
public class DomBindingSerializerJaxpTest extends DomBindingSerializerTest {

	// empty class
}
