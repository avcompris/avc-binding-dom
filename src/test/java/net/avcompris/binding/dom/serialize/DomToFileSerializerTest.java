package net.avcompris.binding.dom.serialize;

import static org.apache.commons.io.FileUtils.forceDelete;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.junit.Assert.assertEquals;

import java.io.File;

import net.avcompris.binding.Serializer;
import net.avcompris.binding.dom.impl.DomToFileSerializer;
import net.avcompris.binding.dom.testutil.LoadXmlUtils;

import org.junit.Test;
import org.w3c.dom.Node;

import com.avcompris.util.junit.JUnitUtils;

public class DomToFileSerializerTest {

	@Test
	public void testSerializeSimpleEmpty() throws Exception {

		final File file = JUnitUtils.newTmpFileNamedAfterCurrentTest(".xml");

		if (file.exists()) {
			forceDelete(file);
		}

		final Serializer<Node> serializer = new DomToFileSerializer(file);

		final Node node = LoadXmlUtils.loadXml("<toto></toto>");

		serializer.serialize(node);

		assertEquals("<toto/>", readFileToString(file, UTF_8));
	}

	@Test
	public void testSerializeSimpleAttributes() throws Exception {

		final File file = JUnitUtils.newTmpFileNamedAfterCurrentTest(".xml");

		if (file.exists()) {
			forceDelete(file);
		}

		final Serializer<Node> serializer = new DomToFileSerializer(file);

		final Node node = LoadXmlUtils
				.loadXml("<toto id='901' name='Arthur'></toto>");

		serializer.serialize(node);

		assertEquals("<toto id=\"901\" name=\"Arthur\"/>", readFileToString(
				file, UTF_8));
	}
}
