package net.avcompris.binding.dom.serialize;

import static com.avcompris.util.junit.AvcMatchers.contains;
import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;

import java.io.StringWriter;

import net.avcompris.binding.Binding;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;
import net.avcompris.binding.dom.impl.DomToWriterSerializer;
import net.avcompris.binding.dom.testutil.LoadXmlUtils;

import org.junit.Test;
import org.w3c.dom.Node;

@DomBinderImplClass(DefaultDomBinder.class)
public class DomBindingSerializerTest extends AbstractDomBinderTest {

	private final StringWriter sw = new StringWriter();

	@Test
	public void testSimple() throws Exception {

		final Node document = LoadXmlUtils
				.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document, Toto.class).useSerializer(
				new DomToWriterSerializer(sw));

		// <toto id="aaa"/>

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final Toto toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());

		//assertSame(toto, toto2);

		toto.serialize();

		assertEquals("<toto id=\"ccc\"/>", sw.toString());
	}

	@Test
	public void testBindingSimpleSame() throws Exception {

		final Node document = LoadXmlUtils.loadXml("<toto id='aaa'/>");

		final Toto toto = getBinder().bind(document, Toto.class);

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		final Toto toto2 = toto.setMyId("ccc");

		assertEquals("ccc", toto.getId());

		assertEquals("ccc", toto2.getId());

		assertEquals(toto.getClass(), toto2.getClass());

		assertSame(toto, toto2);

		assertSame(toto, toto.self());

		assertSame(toto, toto.self(Toto.class));

		assertSame(toto.toNode(), toto2.toNode());

		assertSame(toto.node(), toto2.node());

		assertSame(toto.node(), toto.toNode());

		assertSame(toto.node(), toto.toSelfAny());

		assertSame(toto.node(), toto.toSelfToto());
	}

	@XPath("/toto")
	private interface Toto extends Binding<Toto> {

		@XPath("@id")
		String getId();

		void setId(String id);

		@XPath("@id")
		Toto setMyId(String id);

		@XPath("concat('[toto:id=', @id, ']')")
		@Override
		String toString();

		@XPath(".")
		Node toNode();

		@XPath("self::toto")
		Node toSelfToto();

		@XPath("self::*")
		Node toSelfAny();
	}

	@Test
	public void testClearEverything() throws Exception {

		final Node document = LoadXmlUtils
				.loadXml(createTmpFileFromCommentsAroundThisMethod());

		final Toto toto = getBinder().bind(document, Toto.class).useSerializer(
				new DomToWriterSerializer(sw));

		// <toto id="aaa">
		//    <line>Hello</line>
		//                        This text here
		//    <line>World!</line>
		// </toto>

		assertEquals("aaa", toto.getId());

		toto.setId("bbb");

		assertEquals("bbb", toto.getId());

		toto.serialize();

		assertThat(sw.toString(), contains("This text here"));

		sw.getBuffer().setLength(0);

		toto.clear().serialize();

		assertEquals("<toto/>", sw.toString());
	}
}
