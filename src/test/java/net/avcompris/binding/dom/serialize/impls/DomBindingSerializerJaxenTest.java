package net.avcompris.binding.dom.serialize.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.serialize.DomBindingSerializerTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class DomBindingSerializerJaxenTest extends DomBindingSerializerTest {

	// empty class
}
