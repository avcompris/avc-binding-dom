package net.avcompris.binding.dom;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static com.google.common.collect.ImmutableList.of;
import static org.apache.commons.lang3.StringUtils.split;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;

import com.avcompris.util.AbstractUtils;

public abstract class AbstractDomBinderTest {

	private DomBinder binder;

	protected final DomBinder getBinder() {

		return binder;
	}

	@Before
	public void setUp() throws Exception {

		final Class<? extends AbstractDomBinderTest> thisClass = getClass();

		final DomBinderImplClass annotation = thisClass
				.getAnnotation(DomBinderImplClass.class);

		if (annotation == null) {
			throw new IllegalStateException(
					"Test class should be annotated with @DomBinderImplClass: "
							+ thisClass.getName());
		}

		final Class<? extends DomBinder> binderImplClass = annotation.value();

		final Class<? extends DomBinderFilter>[] filters = annotation.filters();

		// SANITY CHECKS

		final String thisClassName = thisClass.getSimpleName();

		// annotations => test class name

		for (final String x : of("Jaxen", "Jaxp", "Unique")) {
			for (final Class<?> c : ArrayUtils.add(filters, binderImplClass)) {
				final String cname = c.getName();
				if (cname.toLowerCase().contains(x.toLowerCase())
						&& !thisClassName.toLowerCase().contains(
								x.toLowerCase())) {
					throw new IllegalStateException(
							"Names of test class and DomBinder impl+filters do not seem to match (should both contain: \""
									+ x
									+ "\"): "
									+ cname
									+ ", "
									+ thisClassName);
				}
			}
		}

		// test class name => annotations

		for (final String x : of("Default", "Jaxen", "Jaxp", "Unique")) {
			if (thisClassName.toLowerCase().contains(x.toLowerCase())) {
				if (!AvcArrayUtils.contains("getName.toLowerCase", ArrayUtils
						.add(filters, binderImplClass), Pattern.compile(".*"
						+ x.toLowerCase() + ".*"))) {
					throw new IllegalStateException(
							"Names of test class and DomBinder impl+filters do not seem to match (should both contain: \""
									+ x
									+ "\"): "
									+ thisClassName
									+ ", "
									+ binderImplClass.getName());
				}
			}
		}

		// INSTANTIATION

		binder = binderImplClass.newInstance();

		for (int i = filters.length; i > 0; --i) {

			binder = filters[i - 1].getConstructor(DomBinder.class)
					.newInstance(binder);
		}

	}
}

//TODO put this AvcArrayUtils class in avc-commons-lang or what
abstract class AvcArrayUtils extends AbstractUtils {

	/**
	 * @param expression the method names to call on each item in the array
	 * before launching the comparison. For instance, if
	 * <code>expression</code> is <code>"getName.toLowerCase"</code>,
	 * the expression <code>array[i].getName().toLowerCase()</code> will
	 * be evaluated for each index <code>i</code>.
	 */
	//TODO use an Iterable instead of an array
	public static boolean contains(final String expression,
			final Object[] array, //final Object value) {
			final Pattern pattern) {

		nonNullArgument(expression, "expression");
		nonNullArgument(array, "array");

		final String[] methodNames = split(expression, ".");
		final Method[] methods = new Method[methodNames.length];

		Class<?> itemType = array.getClass().getComponentType();

		for (int i = 0; i < methodNames.length; ++i) {

			final String methodName = methodNames[i];
			final Method method;

			try {

				method = itemType.getMethod(methodName);

			} catch (final NoSuchMethodException e) {
				throw new RuntimeException("Cannot find method " + methodName
						+ "() in ref array: " + array);
			}

			methods[i] = method;

			itemType = method.getReturnType();
		}

		final Class<?> valueType = //
		//		(value == null) ? null : value.getClass();
		String.class;

		if (!String.class.equals(itemType)) {
			//		if (value != null && !itemType.isAssignableFrom(valueType)) {
			throw new IllegalArgumentException("Type do not match: itemType="
					+ itemType.getName() + ", valueType=" + valueType.getName());
		}

		for (int i = 0; i < array.length; ++i) {

			Object item = array[i];

			try {

				for (final Method method : methods) {

					item = method.invoke(item);
				}

			} catch (final IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (final IllegalAccessException e) {
				throw new RuntimeException(e);
			} catch (final InvocationTargetException e) {
				throw new RuntimeException(e);
			}

			if (item == null) {
				continue;
			}

			//			if (item == null) {
			//
			//				if (value == null) {
			//					return true;
			//				}
			//
			//			} else {
			//
			//				if (item.equals(value)) {
			//					return true;
			//				}
			//			}

			if (pattern.matcher(item.toString()).matches()) {
				return true;
			}
		}

		return false;
	}
}
