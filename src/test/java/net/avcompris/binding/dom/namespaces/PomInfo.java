package net.avcompris.binding.dom.namespaces;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;

/**
 * some info retrieved from a POM file. 
 */
@XPath("/pom:project")
@Namespaces("xmlns:pom=http://maven.apache.org/POM/4.0.0")
public interface PomInfo {

	@XPath("pom:artifactId")
	String getArtifactId();

	@XPath("ancestor-or-self::pom:project/pom:artifactId")
	String getRootArtifactId();

	@XPath("descendant::pom:version")
	String[] getAllVersions();

	@XPath("pom:repositories/pom:repository")
	PomRepositoryInfo[] getRepositories();

	@XPath(value = "pom:build/pom:plugins/pom:plugin/pom:configuration/*", function = "name()")
	String[] getAllConfigurationPropertyNames();

	@XPath("pom:build/pom:plugins/pom:plugin/pom:configuration/*")
	PomPropertyInfo[] getAllConfigurationProperties();
	
	@XPath("pom:dependencies/pom:dependency")
	PomDependency[] getDependencies();
	
	interface PomDependency {
		
		@XPath("pom:version")
		String getVersion();
	}
}
