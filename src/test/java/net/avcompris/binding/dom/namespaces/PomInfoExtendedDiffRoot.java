package net.avcompris.binding.dom.namespaces;

import net.avcompris.binding.annotation.XPath;


/**
 * extension of PomInfo: Can we override "<code>XPath</code>"?
 */
@XPath("/pom:project//pom:dependency[1]")
public interface PomInfoExtendedDiffRoot extends PomInfo {

	@Override
	String getArtifactId();
}
