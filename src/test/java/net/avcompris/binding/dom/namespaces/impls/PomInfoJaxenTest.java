package net.avcompris.binding.dom.namespaces.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.namespaces.PomInfoTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class PomInfoJaxenTest extends PomInfoTest {

	// empty class
}
