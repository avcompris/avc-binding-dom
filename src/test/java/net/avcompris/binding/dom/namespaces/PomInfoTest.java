package net.avcompris.binding.dom.namespaces;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;

@DomBinderImplClass(DefaultDomBinder.class)
public class PomInfoTest extends AbstractDomBinderTest {

	@Test
	public void test_getArtifactId() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals("avc-binding-dom", pomInfo.getArtifactId());
	}

	@Test
	public void test_getAllVersions() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals(3, pomInfo.getAllVersions().length);
		assertEquals("0.0.1-SNAPSHOT", pomInfo.getAllVersions()[0]);
		assertEquals("0.0.10-SNAPSHOT", pomInfo.getAllVersions()[1]);
		assertEquals("0.0.11-SNAPSHOT", pomInfo.getAllVersions()[2]);
	}

	@Test
	public void test_getRepositories() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals(2, pomInfo.getRepositories().length);
		assertEquals("avcompris-work-snapshots", pomInfo.getRepositories()[0]
				.getId());
		assertEquals("avcompris-work", pomInfo.getRepositories()[1].getId());
	}

	@Test
	public void test_getAllConfigurationPropertyNames() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals(2, pomInfo.getAllConfigurationPropertyNames().length);
		assertEquals("username", pomInfo.getAllConfigurationPropertyNames()[0]);
		assertEquals("password", pomInfo.getAllConfigurationPropertyNames()[1]);
	}

	@Test
	public void test_getAllConfigurationProperties() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals(2, pomInfo.getAllConfigurationProperties().length);
		assertEquals("username", pomInfo.getAllConfigurationProperties()[0]
				.getName());
		assertEquals("${developer-id}",
				pomInfo.getAllConfigurationProperties()[0].getValue());
		assertEquals("password", pomInfo.getAllConfigurationProperties()[1]
				.getName());
		assertEquals("${svn-password}",
				pomInfo.getAllConfigurationProperties()[1].getValue());
	}

	@Test
	public void testInheritanceClassNamespaces() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfoExtended.class);

		assertEquals(2, pomInfo.getAllConfigurationProperties().length);
		assertEquals("username", pomInfo.getAllConfigurationProperties()[0]
				.getName());
	}

	@Test
	public void testInheritanceClassXPath() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"),
				PomInfoExtendedDiffRoot.class);

		assertEquals("avc-common", pomInfo.getArtifactId());
		assertEquals("avc-binding-dom", pomInfo.getRootArtifactId());
	}

	@Test
	public void testInheritanceMethodName() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfoExtended.class);

		assertEquals("avc-binding-dom", pomInfo.getArtifactId());
		assertEquals("avc-binding-dom", pomInfo.getRootArtifactId());
		assertEquals(2, pomInfo.getAllConfigurationProperties().length);
	}

	@Test
	public void testInheritanceMethodNameDiff() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"),
				PomInfoExtendedDiffField.class);

		assertEquals("[avc-binding-dom]", pomInfo.getArtifactId());
		assertEquals("avc-binding-dom", pomInfo.getRootArtifactId());
		assertEquals(2, pomInfo.getAllConfigurationProperties().length);
	}

	@Test
	public void testInnerInterface() throws Exception {

		final PomInfo pomInfo = getBinder().bind(
				loadXml("src/test/xml/002-old-pom.xml"), PomInfo.class);

		assertEquals("0.0.11-SNAPSHOT", pomInfo.getDependencies()[0]
				.getVersion());
	}
}
