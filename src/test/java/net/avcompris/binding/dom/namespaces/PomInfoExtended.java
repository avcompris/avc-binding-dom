package net.avcompris.binding.dom.namespaces;


/**
 * extension of PomInfo: Do we inherit "<code>XPath</code>" and
 * "<code>Namespaces</code>"?
 */
public interface PomInfoExtended extends PomInfo {

	@Override
	String getArtifactId();
}
