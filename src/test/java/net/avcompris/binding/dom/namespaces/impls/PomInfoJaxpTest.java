package net.avcompris.binding.dom.namespaces.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.namespaces.PomInfoTest;

@DomBinderImplClass(JaxpDomBinder.class)
public class PomInfoJaxpTest extends PomInfoTest {

	// empty class
}
