package net.avcompris.binding.dom.namespaces;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;

/**
 * some info retrieved from a POM file. 
 */
@XPath("/pom:project/pom:properties/*")
@Namespaces({
		"pom", "http://maven.apache.org/POM/4.0.0"
})
public interface PomPropertyInfo {

	@XPath(value = "self::*", function = "name()")
	String getName();

	@XPath(".")
	String getValue();
}
