package net.avcompris.binding.dom.namespaces;

import net.avcompris.binding.annotation.XPath;


/**
 * extension of PomInfo: Do we inherit "<code>XPath</code>" and
 * "<code>Namespaces</code>"?
 */
public interface PomInfoExtendedDiffField extends PomInfo {

	@Override
	@XPath("concat('[', pom:artifactId, ']')")
	String getArtifactId();
}
