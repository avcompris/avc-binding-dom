package net.avcompris.binding.dom.equals;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.dom.equals.FetchedLogsWithEquals.FetchedRequestWithEquals;
import net.avcompris.binding.dom.helper.DomBinderUtils;

import org.junit.Test;

public class EqualsTest {

	@Test
	public void testEquals() throws IOException {

		final FetchedLogsWithEquals logs0 = DomBinderUtils.xmlContentToJava(
				new File("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithEquals.class);

		final FetchedRequestWithEquals[] requests0 = logs0.getRequests();

		final int requestCount = requests0.length;

		requests0[0].hashCode();
		
		assertEquals("5_9", requests0[0].toString());
		assertEquals("8_12", requests0[3].toString());

		for (final FetchedRequestWithEquals request : requests0) {

			assertEquals(request.toString(), //
					request.toString(), request.toString());

			assertEquals(request.toString(), request, request);

			assertEquals(request.toString(), //
					request.hashCode(), request.hashCode());
		}

		final FetchedLogsWithEquals logs1 = DomBinderUtils.xmlContentToJava(
				new File("src/test/xml/001-fetched-logs.xml"),
				FetchedLogsWithEquals.class);

		final FetchedRequestWithEquals[] requests1 = logs1.getRequests();

		assertEquals(requestCount, requests1.length);

		for (int i = 0; i < requestCount; ++i) {

			final FetchedRequestWithEquals request0 = requests0[i];
			final FetchedRequestWithEquals request1 = requests1[i];

			assertEquals(request0.toString(), //
					request0.toString(), request1.toString());

			assertEquals(request0.toString(), request0, request1);

			assertEquals(request0.toString(), //
					request0.hashCode(), request1.hashCode());
		}
	}
}
