package net.avcompris.binding.dom.equals;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.logs.FetchedLogLine;

/**
 * this interface represents log lines fetched via the Adm Web Service.
 * 
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
public interface FetchedLogsWithEquals {

	@XPath("requests/request")
	FetchedRequestWithEquals[] getRequests();

	interface FetchedRequestWithEquals {

		// @Override
		// int hashCode(); // Note: If not overridden, will rely on #toString() 
		
		@Override
		@XPath("concat(@id, '_', count(log))")
		String toString();

		@XPath("@id")
		int getId();

		@XPath("log")
		FetchedLogLine[] getLogLines();

		int sizeOfLogLines();
	}
}
