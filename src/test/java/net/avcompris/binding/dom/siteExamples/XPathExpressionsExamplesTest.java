package net.avcompris.binding.dom.siteExamples;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;

import javax.xml.parsers.DocumentBuilderFactory;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;
import org.w3c.dom.Document;

/**
 * tests that match the APT documentation.
 * 
 * @author David Andrianavalontsalama
 */
public class XPathExpressionsExamplesTest {

	@Test
	public void testXPathExpressions_example001() throws Exception {

		final Document document = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder().parse(
						createTmpFileFromCommentsAroundThisMethod());

		final DomBinder binder = new DefaultDomBinder();

		final Book book = binder.bind(document, Book.class);

		assertEquals(19, book.getPublicationCentury());
	}

	// <book title="Treasure Island">
	// <reference url="http://en.wikipedia.org/wiki/Treasure_Island" lang="en"/>
	// <reference url="http://fr.wikipedia.org/wiki/L'Île_au_trésor" lang="fr"/>
	// <publishedIn>1883</publishedIn>
	// <author>
	// <name>Robert Louis Stevenson</name>
	// <name>R. L. Stevenson</name>
	// </author>
	// </book>

	@XPath("/book")
	public interface Book {

		@XPath(value = "publishedIn", function = "floor(. div 100) + 1")
		int getPublicationCentury();
	}
}
