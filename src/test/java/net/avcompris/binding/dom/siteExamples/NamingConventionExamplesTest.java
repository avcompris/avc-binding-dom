package net.avcompris.binding.dom.siteExamples;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;
import org.w3c.dom.Document;

/**
 * tests that match the APT documentation.
 * 
 * @author David Andrianavalontsalama
 */
public class NamingConventionExamplesTest {

	@Test
	public void testNamingConventions_example001() throws Exception {

		final Document document = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder().parse(
						createTmpFileFromCommentsAroundThisMethod());

		final DomBinder binder = new DefaultDomBinder();

		final Books books = binder.bind(document, Books.class);

		//<books>
		//   <book title="Treasure Island" lang="en">
		//       <author> Robert    Louis Stevenson</author>
		//   </book>
		//   <book title="L'Île au trésor" lang="fr">
		//   </book>
		//</books>

		assertEquals("Treasure Island", books.getBooks()[0].getTitle());
		assertEquals(" Robert    Louis Stevenson", books.getBooks()[0]
				.getAuthor());
		assertFalse(books.getBooks()[0].isNullAuthor());
		assertEquals(1, books.getBooks()[0].sizeOfAuthor());
		assertEquals(26, books.getBooks()[0].getAuthorLength());
		assertEquals(26, books.getBooks()[0].getAuthorLengthSafe());
		assertEquals("Robert Louis Stevenson", books.getBooks()[0]
				.getAuthorNormalized());

		assertEquals("L'Île au trésor", books.getBooks()[1].getTitle());
		assertEquals("", books.getBooks()[1].getAuthor());
		assertTrue(books.getBooks()[1].isNullAuthor());
		assertEquals(0, books.getBooks()[1].sizeOfAuthor());
		assertEquals(0, books.getBooks()[1].getAuthorLengthSafe());
		assertEquals("", books.getBooks()[1].getAuthorNormalized());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNamingConventions_example001_crash() throws Exception {

		final Document document = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(
						createTmpFileFromCommentsAroundThisMethod("testNamingConventions_example001"));

		final DomBinder binder = new DefaultDomBinder();

		final Books books = binder.bind(document, Books.class);

		books.getBooks()[1].getAuthorLength(); // kaboom!
	}

	@XPath("/books")
	interface Books {

		@XPath("book")
		Book[] getBooks();

		interface Book {

			@XPath("@title")
			String getTitle();

			@XPath("@lang")
			String getLang();

			@XPath("author")
			String getAuthor();

			boolean isNullAuthor();

			int sizeOfAuthor();

			@XPath(value = "author", function = "normalize-space()")
			String getAuthorNormalized();

			@XPath(value = "author", function = "string-length()")
			int getAuthorLength();

			@XPath(value = "author", function = "string-length()", failFunction = "0")
			int getAuthorLengthSafe();
		}
	}

	@Test
	public void testNamingConventions_example002() throws Exception {

		final Document document = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder().parse(
						createTmpFileFromCommentsAroundThisMethod());

		final DomBinder binder = new DefaultDomBinder();

		final Map<String, BooksWithMap.Book> books = binder.bind(document,
				BooksWithMap.class).getBooks();

		//<books>
		//   <book title="Treasure Island" lang="en">
		//       <author> Robert    Louis Stevenson</author>
		//   </book>
		//   <book title="L'Île au trésor" lang="fr">
		//       <author> R. L.           Stevenson</author>
		//   </book>
		//</books>

		assertEquals(2, books.size());
		assertEquals("Treasure Island", books.get("Treasure Island").getTitle());

		final Map<String, BooksWithMap.Book> booksComplicated = books.values()
				.iterator().next().getParent().getBooksComplicated();

		assertEquals(2, booksComplicated.size());
		assertEquals("Treasure Island", booksComplicated.get("1.en").getTitle());
		assertEquals("L'Île au trésor", booksComplicated.get("2.fr").getTitle());
	}

	@XPath("/books")
	public interface BooksWithMap {

		@XPath(value = "book", //
		mapKeysXPath = "@title", mapKeysType = String.class, //
		mapValuesXPath = ".", mapValuesType = Book.class)
		Map<String, Book> getBooks();

		@XPath(value = "book", //
		mapKeysXPath = ".", //
		mapKeysFunction = "concat(1 + count(preceding-sibling::book), '.', @lang)", //
		mapKeysType = String.class, //
		mapValuesXPath = ".", mapValuesType = Book.class)
		Map<String, Book> getBooksComplicated();

		interface Book {

			@XPath("@title")
			String getTitle();

			@XPath("@lang")
			String getLang();

			@XPath("author")
			String getAuthor();

			@XPath("..")
			BooksWithMap getParent();
		}
	}
}
