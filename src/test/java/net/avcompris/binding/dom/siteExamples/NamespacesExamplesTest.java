package net.avcompris.binding.dom.siteExamples;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;

import javax.xml.parsers.DocumentBuilderFactory;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.DomBinder;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;
import org.w3c.dom.Document;

/**
 * tests that match the APT documentation.
 * 
 * @author David Andrianavalontsalama
 */
public class NamespacesExamplesTest {

	@Test
	public void testGettingStarted_example001() throws Exception {

		final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		final Document document = documentBuilderFactory.newDocumentBuilder()
				.parse(createTmpFileFromCommentsAroundThisMethod());

		final DomBinder binder = new DefaultDomBinder();

		final Book book = binder.bind(document, Book.class);

		assertEquals("Treasure Island", book.getTitle());
		assertEquals(1883, book.getPublishYear());
		assertEquals(2, book.getAuthorNames().length);
		assertEquals("Robert Louis Stevenson", book.getAuthorNames()[0]);
		assertEquals("R. L. Stevenson", book.getAuthorNames()[1]);
		assertEquals(2, book.getReferences().length);
		assertEquals("en", book.getReferences()[0].getLang());
		assertEquals("fr", book.getReferences()[1].getLang());
	}

	// <book title="Treasure Island" xmlns="http://mybook">
	// <reference url="http://en.wikipedia.org/wiki/Treasure_Island" lang="en"/>
	// <reference url="http://fr.wikipedia.org/wiki/L'Île_au_trésor" lang="fr"/>
	// <publishedIn>1883</publishedIn>
	// <author xmlns:people="a/b/c/">
	// <people:name>Robert Louis Stevenson</people:name>
	// <people:name>R. L. Stevenson</people:name>
	// </author>
	// </book>

	@Namespaces({
			"xmlns:m=http://mybook", "xmlns:people=a/b/c/"
	})
	@XPath("/m:book")
	public interface Book {

		@XPath("@title")
		String getTitle();

		@XPath("m:publishedIn")
		int getPublishYear();

		@XPath("m:author/people:name")
		String[] getAuthorNames();

		@XPath("m:reference")
		MyBookReference[] getReferences();

		interface MyBookReference {

			@XPath("@url")
			String getUrl();

			@XPath("@lang")
			String getLang();
		}
	}
}
