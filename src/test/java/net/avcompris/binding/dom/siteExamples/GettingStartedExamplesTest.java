package net.avcompris.binding.dom.siteExamples;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;

import javax.xml.parsers.DocumentBuilderFactory;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.helper.DomBinderUtils;

import org.junit.Test;
import org.w3c.dom.Document;

/**
 * tests that match the APT documentation.
 * 
 * @author David Andrianavalontsalama
 */
public class GettingStartedExamplesTest {

	@Test
	public void testGettingStarted_example001() throws Exception {

		final Document document = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder().parse(
						createTmpFileFromCommentsAroundThisMethod());

		/*
		 * This is equivalent to:
		 * final DomBinder binder = new DefaultDomBinder();
		 * final Book book = binder.bind(document, Book.class); 
		 *
		 */		
		final Book book = DomBinderUtils.xmlContentToJava(document, Book.class);

		assertEquals("Treasure Island", book.getTitle());
		assertEquals(1883, book.getPublishYear());
		assertEquals(2, book.getAuthorNames().length);
		assertEquals("Robert Louis Stevenson", book.getAuthorNames()[0]);
		assertEquals("R. L. Stevenson", book.getAuthorNames()[1]);
		assertEquals(2, book.getReferences().length);
		assertEquals("en", book.getReferences()[0].getLang());
		assertEquals("fr", book.getReferences()[1].getLang());
	}

	// <book title="Treasure Island">
	//    <reference url="http://en.wikipedia.org/wiki/Treasure_Island" lang="en"/>
	//    <reference url="http://fr.wikipedia.org/wiki/L'Île_au_trésor" lang="fr"/>
	//    <publishedIn>1883</publishedIn>
	//    <author>
	//       <name>Robert Louis Stevenson</name>
	//       <name>R. L. Stevenson</name>
	//    </author>
	// </book>

	@XPath("/book")
	public interface Book {

		@XPath("@title")
		String getTitle();

		@XPath("publishedIn")
		int getPublishYear();

		@XPath("author/name")
		String[] getAuthorNames();

		@XPath("reference")
		MyBookReference[] getReferences();

		interface MyBookReference {

			@XPath("@url")
			String getUrl();

			@XPath("@lang")
			String getLang();
		}
	}
}
