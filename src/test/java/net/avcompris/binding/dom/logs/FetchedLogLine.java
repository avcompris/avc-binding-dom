package net.avcompris.binding.dom.logs;

import net.avcompris.binding.annotation.XPath;

import org.w3c.dom.Node;

/**
 * this interface represents a log line fetched via the Adm Web Service.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/request/log")
public interface FetchedLogLine {

	@XPath("@range")
	int getRange();

	@XPath("@level")
	char getLevel();

	@XPath("@elapsedMs")
	int getElapsedMs();

	@XPath("@msgTruncated")
	String getMsgTruncated();
	
	@XPath("concat('request_', parent::request/@id, '_log_', @range)")
	@Override
	String toString();
	
	@XPath(".")
	Node toNode();	
	
	// @XPath("generate-id()") // Jaxen doesn't have the generate-id()
	@XPath("concat(count(ancestor-or-self::*), '_', count(preceding::*))")
	String getUUID();
}
