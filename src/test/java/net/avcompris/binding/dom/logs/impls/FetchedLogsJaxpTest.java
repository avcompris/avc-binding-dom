package net.avcompris.binding.dom.logs.impls;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxpDomBinder;
import net.avcompris.binding.dom.logs.FetchedLogLine;
import net.avcompris.binding.dom.logs.FetchedLogs;
import net.avcompris.binding.dom.logs.FetchedLogsTest;

import org.junit.Test;

@DomBinderImplClass(JaxpDomBinder.class)
public class FetchedLogsJaxpTest extends FetchedLogsTest {

	@Test
	public void test_Aliases_equals_UUID() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		assertEquals(63, allLogLines.length);

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		assertEquals(log_0, log_0_0);
	}
}
