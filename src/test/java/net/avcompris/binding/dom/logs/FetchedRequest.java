package net.avcompris.binding.dom.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents a request fetched via the Adm Web Service.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/request")
public interface FetchedRequest {

	@XPath("@id")
	int getId();

	@XPath("@at")
	DateTime getAt();

	@XPath("@elapsedMs")
	int getElapsedMs();

	boolean isNullElapsedMs();

	@XPath("@sessionId")
	int getSessionId();

	boolean isNullSessionId();

	@XPath("log")
	FetchedLogLine[] getLogLines();

	int sizeOfLogLines();
}
