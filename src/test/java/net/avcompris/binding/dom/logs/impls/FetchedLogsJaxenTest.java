package net.avcompris.binding.dom.logs.impls;

import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.JaxenDomBinder;
import net.avcompris.binding.dom.logs.FetchedLogsTest;

@DomBinderImplClass(JaxenDomBinder.class)
public class FetchedLogsJaxenTest extends FetchedLogsTest {

	// empty class
}
