package net.avcompris.binding.dom.logs;

import static net.avcompris.binding.dom.testutil.LoadXmlUtils.loadXml;
import static org.joda.time.DateTimeZone.UTC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.dom.AbstractDomBinderTest;
import net.avcompris.binding.dom.DomBinderImplClass;
import net.avcompris.binding.dom.impl.DefaultDomBinder;
import net.avcompris.binding.dom.impl.DomUniqueBinder;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Node;

@DomBinderImplClass(DefaultDomBinder.class)
public class FetchedLogsTest extends AbstractDomBinderTest {

	@Test
	public final void test_getLogCount() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		assertEquals(375, logs.getCount());
	}

	@Test
	public final void test_getLogCounts_arrayOfInts() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final int[] counts = logs.getCounts();

		assertEquals(1, counts.length);

		assertEquals(375, counts[0]);
	}

	@Test
	public final void test_otherXPath_getLogCount() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(BindConfiguration.newBuilder("/logs/requests").build(),
						loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		assertEquals(38, logs.getCount());
	}

	@Test
	public final void test_int_function() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		assertEquals(6, logs.getActualRequestCount());

		assertEquals(6, logs.sizeOfRequests());
	}

	@Test
	public final void test_otherXPath_getMsgTruncated() throws Exception {

		final FetchedLogLine logLine = getBinder().bind(
				BindConfiguration.newBuilder(
						"/logs/requests/request[1]/log[@range = 4]").build(),
				loadXml("src/test/xml/001-fetched-logs.xml"),
				FetchedLogLine.class);

		assertEquals(4, logLine.getRange());
		assertEquals('I', logLine.getLevel());
		assertEquals(1710, logLine.getElapsedMs());
		assertEquals("HTTP_HOST: localhost", logLine.getMsgTruncated());
	}

	@Test
	public final void test_getDateTime() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		assertEquals(new DateTime(2011, 11, 25, 0, 50, 4, UTC),
				logs.getRequestMinRequestAt());
	}

	@Test
	public final void test_otherXPath_hasElapsedMs() throws Exception {

		final FetchedRequest request5 = getBinder().bind(
				BindConfiguration.newBuilder("/logs/requests/request[@id = 5]")
						.build(), loadXml("src/test/xml/001-fetched-logs.xml"),
				FetchedRequest.class);

		assertEquals(5, request5.getId());
		assertFalse(request5.isNullElapsedMs());
		assertEquals(1903, request5.getElapsedMs());

		final FetchedRequest request8 = getBinder().bind(
				BindConfiguration.newBuilder("/logs/requests/request[@id = 8]")
						.build(), loadXml("src/test/xml/001-fetched-logs.xml"),
				FetchedRequest.class);

		assertEquals(8, request8.getId());
		assertTrue(request8.isNullElapsedMs());
		assertEquals(0, request8.getElapsedMs());
	}

	@Test
	public final void test_sizeOfRequests() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		assertEquals(6, logs.sizeOfRequests());
	}

	@Test
	public final void test_getRequests() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(8, requests[3].getId());
	}

	@Test
	public final void test_getRequests_sizeOfLogLines() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(9, requests[0].sizeOfLogLines());

		assertEquals(8, requests[3].getId());
		assertEquals(12, requests[3].sizeOfLogLines());
	}

	@Test
	public final void test_getRequests_getLogLines() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals("PHP_SELF: /tmp2/index.php",
				requests[0].getLogLines()[8].getMsgTruncated());

		assertEquals(8, requests[3].getId());
		assertEquals("PHP_SELF: /tmp2/404.php",
				requests[3].getLogLines()[8].getMsgTruncated());
	}

	@Test
	public final void test_toString() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		assertEquals(63, allLogLines.length);

		final FetchedLogLine log_0 = allLogLines[0];

		assertEquals("request_5_log_1", log_0.toString());

		System.out.println(log_0);
	}

	@Test
	public final void test_Aliases_getNode_same() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		final Node node_0 = log_0.toNode();

		final Node node_0_0 = log_0_0.toNode();

		assertEquals(node_0, node_0_0);

		assertSame(node_0, node_0_0);
	}

	@Test
	public final void test_Aliases_same() throws Exception {

		final FetchedLogs logs = new DomUniqueBinder(getBinder())
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		//System.out.println(log_0.getUUID());

		//System.out.println(log_0_0.getUUID());

		assertSame(log_0, log_0_0);
	}

	@Test
	public final void test_Aliases_notSame() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		//System.out.println(log_0.getUUID());

		//System.out.println(log_0_0.getUUID());

		assertNotSame(log_0, log_0_0);
	}

	@Test
	@Ignore
	// would crash with Jaxen because of "generate-id()"
	public void test_Aliases_equals() throws Exception {

		final FetchedLogs logs = getBinder()
				.bind(loadXml("src/test/xml/001-fetched-logs.xml"),
						FetchedLogs.class);

		final FetchedLogLine[] allLogLines = logs.getAllLogLines();

		assertEquals(63, allLogLines.length);

		final FetchedLogLine log_0 = allLogLines[0];

		final FetchedLogLine log_0_0 = logs.getRequests()[0].getLogLines()[0];

		assertEquals(log_0, log_0_0);
	}
}
