package net.avcompris.binding.dom.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents log lines fetched via the Adm Web Service.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
public interface FetchedLogs {

	@XPath("@count")
	int getCount();

	@XPath("@count")
	int[] getCounts();

	@XPath("requests/@count")
	int getRequestCount();

	@XPath("requests/@minId")
	int getRequestMinId();

	boolean isNullRequestMinId();

	@XPath("requests/@minRequestAt")
	DateTime getRequestMinRequestAt();

	@XPath("requests/@maxId")
	int getRequestMaxId();

	boolean isNullRequestMaxId();

	@XPath("requests/@maxRequestAt")
	DateTime getRequestMaxRequestAt();

	@XPath("sessions/@count")
	int getSessionCount();

	@XPath("requests/request")
	FetchedRequest[] getRequests();

	int sizeOfRequests();

	@XPath(value = "requests", function = "count(request)")
	int getActualRequestCount();
	
	@XPath("requests/request/log")
	FetchedLogLine[] getAllLogLines();
	
	@XPath("sum(descendant-or-self::*/@elapsedMs) * 123456789")
	int getIntTotalElapsedMsTimes123456789();
	
	@XPath("sum(descendant-or-self::*/@elapsedMs) * 123456789")
	long getLongTotalElapsedMsTimes123456789();
}
