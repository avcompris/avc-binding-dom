package net.avcompris.binding.sax.impl;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.dom.impl.DefaultDomBinder;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class DefaultSAXBinderTest {

	@Test
	public void testSimple_SAX() throws Exception {

		final Book book;

		final InputStream is = new FileInputStream(
				createTmpFileFromCommentsAroundThisMethod());
		try {

			book = new DefaultSAXBinder().bind(new InputSource(is), Book.class);

		} finally {
			is.close();
		}

		//<book title="Treasure Island">
		// <reference url="http://en.wikipedia.org/wiki/Treasure_Island" lang="en"/>
		// <reference url="http://fr.wikipedia.org/wiki/L'Île_au_trésor" lang="fr"/>
		// <publishedIn>1883</publishedIn>
		// <author>
		//    <name>Robert Louis Stevenson</name>
		//    <name>R. L. Stevenson</name>
		// </author>
		//</book>

		assertEquals("Treasure Island", book.getTitle());
		assertEquals(1883, book.getPublishYear());
		assertEquals(2, book.getAuthorNames().length);
		assertEquals("Robert Louis Stevenson", book.getAuthorNames()[0]);
		assertEquals("R. L. Stevenson", book.getAuthorNames()[1]);
		assertEquals(2, book.getReferences().length);
		assertEquals("en", book.getReferences()[0].getLang());
		assertEquals("fr", book.getReferences()[1].getLang());
	}

	@XPath("/book")
	private interface Book {

		@XPath("@title")
		String getTitle();

		@XPath("publishedIn")
		int getPublishYear();

		@XPath("author/name")
		String[] getAuthorNames();

		@XPath("reference")
		MyBookReference[] getReferences();

		interface MyBookReference {

			@XPath("@url")
			String getUrl();

			@XPath("@lang")
			String getLang();
		}
	}

	@Test
	public void testSubTextNodes_SAX() throws Exception {

		final Book book;

		final InputStream is = new FileInputStream(
				createTmpFileFromCommentsAroundThisMethod());
		try {

			book = new DefaultSAXBinder().bind(new InputSource(is), Book.class);

		} finally {
			is.close();
		}

		//<book title="Treasure Island">
		// <publishedIn><century>18</century><decade>83</decade></publishedIn>
		//</book>

		assert_testSubTextNodes(book);
	}

	private static void assert_testSubTextNodes(final Book book) {

		assertEquals("Treasure Island", book.getTitle());
		assertEquals(0, book.getAuthorNames().length);
		assertEquals(0, book.getReferences().length);
		assertEquals(1883, book.getPublishYear());

	}

	@Test
	public void testSubTextNodes_DOM() throws Exception {

		final Document document = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(
						createTmpFileFromCommentsAroundThisMethod("testSubTextNodes_SAX"));

		final Book book = new DefaultDomBinder().bind(document, Book.class);

		assert_testSubTextNodes(book);
	}

	@Test
	public void testMultipleTextNodes_SAX() throws Exception {

		final Book book;

		final InputStream is = new FileInputStream(
				createTmpFileFromCommentsAroundThisMethod());
		try {

			book = new DefaultSAXBinder().bind(new InputSource(is), Book.class);

		} finally {
			is.close();
		}

		//<book title="Treasure_Island">
		// <publishedIn>18</publishedIn>
		// <publishedIn>83</publishedIn>
		//</book>

		assert_testMultipleTextNodes(book);
	}

	private static void assert_testMultipleTextNodes(final Book book) {

		assertEquals("Treasure_Island", book.getTitle());
		assertEquals(0, book.getAuthorNames().length);
		assertEquals(0, book.getReferences().length);
		assertEquals(18, book.getPublishYear());

	}

	@Test
	public void testMultipleTextNodes_DOM() throws Exception {

		final Document document = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(
						createTmpFileFromCommentsAroundThisMethod("testMultipleTextNodes_SAX"));

		final Book book = new DefaultDomBinder().bind(document, Book.class);

		assert_testMultipleTextNodes(book);
	}

	@Test
	public void testEmptyString_SAX() throws Exception {

		final Book book;

		final InputStream is = new FileInputStream(
				createTmpFileFromCommentsAroundThisMethod());
		try {

			book = new DefaultSAXBinder().bind(new InputSource(is), Book.class);

		} finally {
			is.close();
		}

		//<book x-title="Treasure Island">
		// <author>
		//    <name>Robert Louis Stevenson</name>
		// </author>
		//</book>

		assert_testEmptyString(book);
	}

	private static void assert_testEmptyString(final Book book) {

		assertEquals("", book.getTitle());
		assertEquals(1, book.getAuthorNames().length);

	}

	@Test
	public void testEmptyString_DOM() throws Exception {

		final Document document = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(
						createTmpFileFromCommentsAroundThisMethod("testEmptyString_SAX"));

		final Book book = new DefaultDomBinder().bind(document, Book.class);

		assert_testEmptyString(book);
	}

	@Test
	public void testStreetAliases_same() throws Exception {

		final City city;

		final InputStream is = new FileInputStream(
				createTmpFileFromCommentsAroundThisMethod());
		try {

			city = new DefaultSAXBinder().bind(new InputSource(is), City.class);

		} finally {
			is.close();
		}

		//<city name='Limoges'>
		// <street name='Rue Bertheau'>
		//  <address number='24b'/>
		//  <address number='37'/>
		//  <address number='36b'/>
		// </street>
		// <street name='Rue Naturel'>
		//  <address number='12'/>
		//  <address number='1080'/>
		// </street>
		//</city>

		assertEquals("Limoges", city.getName());
		assertEquals(2, city.getStreets().length);
		assertEquals("Rue Bertheau", city.getStreets()[0].getName());
		assertEquals(3, city.getStreets()[0].getAddresses().length);
		assertEquals("24b", city.getStreets()[0].getAddresses()[0].getNumber());
		assertEquals("37", city.getStreets()[0].getAddresses()[1].getNumber());
		assertEquals("36b", city.getStreets()[0].getAddresses()[2].getNumber());
		assertEquals("Rue Naturel", city.getStreets()[1].getName());
		assertEquals(2, city.getStreets()[1].getAddresses().length);
		assertEquals("12", city.getStreets()[1].getAddresses()[0].getNumber());
		assertEquals("1080", city.getStreets()[1].getAddresses()[1].getNumber());

		assertEquals(5, city.getAllAddresses().length);
		assertEquals("24b", city.getAllAddresses()[0].getNumber());
		assertEquals("37", city.getAllAddresses()[1].getNumber());
		assertEquals("36b", city.getAllAddresses()[2].getNumber());
		assertEquals("12", city.getAllAddresses()[3].getNumber());
		assertEquals("1080", city.getAllAddresses()[4].getNumber());

		assertSame(city.getStreets()[0].getAddresses()[0], city
				.getAllAddresses()[0]);
		assertSame(city.getStreets()[0].getAddresses()[2], city
				.getAllAddresses()[2]);
		assertSame(city.getStreets()[1].getAddresses()[0], city
				.getAllAddresses()[3]);
	}

	@XPath("/city")
	private interface City {

		@XPath("@name")
		String getName();

		@XPath("street/address")
		Street.Address[] getAllAddresses();

		@XPath("street")
		Street[] getStreets();

		interface Street {

			@XPath("@name")
			String getName();

			@XPath("address")
			Address[] getAddresses();

			interface Address {

				@XPath("@number")
				String getNumber();
			}
		}
	}
}
